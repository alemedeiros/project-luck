# luck.py
# Project - MC911
# luck - Lya Ultra Compiler Kit
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Luck (Lya Ultra Compiler Kit): compiler for the Lya scripting language,
developed for the Compiler Project course at Unicamp by Alexandre Medeiros
(alemedeiros) and Giovanna de Luca (gimdluca).
"""

import argparse
import os
import pprint
import sys

import lucky.lexer
import lucky.parser
import lucky.visitors
import lucky.semantic
import lucky.code
import lucky.vm


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('file', metavar='FILE', type=str,
                        help='Lya source file')
    parser.add_argument('-o', '--output', default='', type=str,
                        help='output file of the compiler')
    parser.add_argument('--print_ast', action='store_true',
                        help='Print program Abstract Syntax Tree')
    parser.add_argument('--ast_format', choices=['magic', 'dot', 'simple'],
                        default='magic', help='Format for the AST output')
    parser.add_argument('-r', '--run', action='store_true',
                        help='Run code after compilation')
    args = parser.parse_args()

    # Open input file
    # TODO(alemedeiros): Add some verification code to make sure file exists
    # and is readable
    f = open(args.file, 'r')

    # Default output file is the input file with changed sufix
    if args.output == '':
        base = os.path.basename(args.file)
        args.output = os.path.splitext(base)[0] + '.out'

    # output prefix for temporary outputs
    prefix = os.path.splitext(args.output)[0]

    # Instantiate compiler stages
    p = lucky.parser.Parser()
    p.build()
    l = lucky.lexer.Lexer()

    # Stage 0: read file
    prgm_txt = f.read()

    # Stage 1: Lexical and Syntactic Analysis
    prgm_ast = p.parse(prgm_txt, l)

    # Stage 1.5: Print Abstract Syntax Tree
    # TODO(alemedeiros): Make visitors print to file
    if args.ast_format == 'magic':
        ast_f = open(prefix + '.ast', 'w')
        v = lucky.visitors.MagicVisitor(ast_f)
        v.visit(prgm_ast)
        ast_f.close()
    elif args.ast_format == 'dot':
        v = lucky.visitors.DotVisitor(prefix)
        v.visit(prgm_ast)
        v.dot.render()
    elif args.ast_format == 'simple':
        v = lucky.visitors.ShowAstVisitor()
        v.visit(prgm_ast, indent=0)

    # Stage 2: Semantical Analysis
    checker = lucky.semantic.SemanticCheckVisitor()
    decorated_ast = checker.visit(prgm_ast)

    if checker.error:
        print(":(")
        sys.exit(1)

    # TODO(any): Stage 3: Code Generation
    code_gen = lucky.code.CodeGen()
    code_gen.visit(decorated_ast)
    print('H')
    for i, item in enumerate(code_gen.h):
        print(i,item,sep=' : ')
    # pprint.pprint(code_gen.h)
    print('Code')
    for i, item in enumerate(code_gen.code):
        print(i,item,sep=' : ')
    #    pprint.pprint(code_gen.code)

    # Run the code
    if args.run:
        print('\n**Running Lya Virtual Machine**')
        vm = lucky.vm.VirtualMachine(code_gen.h, code_gen.code)
        vm.run()


if __name__ == '__main__':
    main()
else:
    print("ERROR: luck compiler should be run as main, not imported!",
          file=sys.stderr)
    print("       For the lucky components, use lucky package.",
          file=sys.stderr)
    sys.exit(1)
