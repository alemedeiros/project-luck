# visitors.py
# Project 1 - MC911
# luck - Lya Ultra Compiler Kit
#
# Lexical & Syntactic Analysis of Lya language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Definition of classes for doing computations over Lucky AST, following the
visitor pattern.
"""

from . import ast
from graphviz import Digraph


# --- Custom generic (node-agnostic) visitor
class MagicVisitor(object):
    """
    Alternative implementation of a printing Visitor (note that this approach
    is a little different than NodeVisitor class approach, therefore it doesn't
    have to be a subclass).
    """

    def __init__(self, buf):
        self.buf = buf

    def visit(self, node, **kwargs):
        # Must have depth at kwargs
        # assert 'depth' in kwargs, 'missing depth argument for MagicVisitor'

        # Call appropriate method
        if isinstance(node, ast.AST):
            self.visit_AST(node, **kwargs)
        elif isinstance(node, (list, tuple)):
            self.visit_list(node, **kwargs)
        else:
            self.visit_value(node, **kwargs)

    def visit_AST(self, node, depth=0):
        indent = '    ' * depth
        # Current node name
        print(node.__class__.__name__, sep='', file=self.buf)
        for f in node._fields:
            # Print field name (with half indent) then its value
            print(indent, '- ', f, ': ', sep='', end='', file=self.buf)
            self.visit(getattr(node, f), depth=depth + 1)
        for opt in node._opt_fields:
            if hasattr(node, opt):
                # Print optional field name then its value
                print(indent, '* ', opt, ': ', sep='', end='', file=self.buf)
                self.visit(getattr(node, opt), depth=depth + 1)

    def visit_list(self, l, depth=0):
        indent = '    ' * depth
        # Print list start bracket
        print('[', sep='', file=self.buf)
        i = 0
        for e in l:
            # Print each element prepended by index in the array
            print(indent, '%2i' % i, ': ', sep='', end='', file=self.buf)
            self.visit(e, depth=depth + 1)
            i += 1
        print(indent, ']', sep='', file=self.buf)

    def visit_value(self, v, **kwargs):
        print(v, sep='', file=self.buf)


class DotVisitor(object):
    # aux id start at 2^20
    aux_id = 1 << 20

    def __init__(self, name):
        self.dot = Digraph(name=name)

    def visit(self, node, **kwargs):
        # Call appropriate method
        if isinstance(node, ast.AST):
            self.visit_AST(node, **kwargs)
        elif isinstance(node, (list, tuple)):
            self.visit_list(node, **kwargs)
        else:
            self.visit_value(node, **kwargs)

    def visit_AST(self, node, parent=-1, edge=None):
        # Current node name
        self.dot.node(str(node.nid), label=node.__class__.__name__)
        if parent != -1:
            self.dot.edge(str(parent), str(node.nid), label=edge)
        for f in node._fields:
            # Print field name (with half indent) then its value
            self.visit(getattr(node, f), parent=node.nid, edge=f)
        for opt in node._opt_fields:
            if hasattr(node, opt):
                # Print optional field name then its value
                self.visit(getattr(node, opt), parent=node.nid,
                           edge='['+opt+']')

    def visit_list(self, l, parent=-1, edge=None):
        my_id = self.aux_id
        self.aux_id += 1
        self.dot.node(str(my_id), label='list')
        if parent != -1:
            self.dot.edge(str(parent), str(my_id), label=edge)
        i = 0
        for e in l:
            self.visit(e, parent=my_id, edge=str(i))
            i += 1

    def visit_value(self, v, parent=-1, edge=None):
        my_id = self.aux_id
        self.aux_id += 1
        self.dot.node(str(my_id), label='value: '+str(v))
        if parent != -1:
            self.dot.edge(str(parent), str(my_id), label=edge)


# ---- AST node visiting classes ----
class NodeVisitor(object):
    """
    Class for visiting nodes of the parse tree.  This is modeled after
    a similar class in the standard library ast.NodeVisitor.  For each
    node, the visit(node) method calls a method visit_NodeName(node)
    which should be implemented in subclasses.  The generic_visit() method
    is called for all nodes where there is no matching visit_NodeName()
    method.
    Here is a example of a visitor that examines binary operators:
    class VisitOps(NodeVisitor):
        visit_Binop(self,node):
            print("Binary operator", node.op)
            self.visit(node.left)
            self.visit(node.right)
        visit_Unaryop(self,node):
            print("Unary operator", node.op)
            self.visit(node.expr)
    tree = parse(txt)
    VisitOps().visit(tree)
    """

    def visit(self, node, **kwargs):
        """
        Execute a method of the form visit_NodeName(node) where
        NodeName is the name of the class of a particular node.
        """
        if node:
            method = 'visit_' + node.__class__.__name__
            visitor = getattr(self, method, self.generic_visit)
            return visitor(node, **kwargs)
        else:
            return None

    def generic_visit(self, node, **kwargs):
        """
        Method executed if no applicable visit_ method can be found.
        This examines the node to see if it has _fields, is a list,
        or can be further traversed.
        """
        for field in getattr(node, '_fields'):
            value = getattr(node, field, None)
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        self.visit(item, **kwargs)
            elif isinstance(value, ast.AST):
                self.visit(value, **kwargs)


class ShowAstVisitor(NodeVisitor):
    """
    Visitor class to print the AST.
    """

    def visit(self, node, **kwargs):
        assert 'indent' in kwargs, 'missing indent argument for ShowAstVisitor'
        print('    ' * kwargs['indent'], end='')
        super().visit(node, **kwargs)

    def visit_Program(self, program, indent=0):
        print('Program')
        for i in program.stmts:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_Program_Node', sep='')

    def visit_DeclarationStatement(self, statement, indent=0):
        print('DeclarationStatement')
        for i in statement.dcls:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_Declaration_Statement', sep='')

    def visit_SynonymStatement(self, statement, indent=0):
        print('SynonymStatement')
        for i in statement.syns:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_SynonymStatement', sep='')

    def visit_NewModeStatement(self, statement, indent=0):
        print('NewModeStatement')
        for i in statement.newmodes:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_NewModeStatement', sep='')

    def visit_ProcedureStatement(self, statement, indent=0):
        print('ProcedureStatement')
        self.visit(statement.label, indent=indent + 1)
        self.visit(statement.definition, indent=indent + 1)
        print('    ' * indent, 'end_ProcedureStatement', sep='')

    def visit_ActionStatement(self, statement, indent=0):
        print('ActionStatement')
        self.visit(statement.action, indent=indent + 1)
        if hasattr(statement, 'label'):
            self.visit(statement.label, indent=indent + 1)
        print('    ' * indent, 'end_ActionStatement', sep='')

    def visit_CustomMode(self, custom, indent=0):
        print('CustomMode')
        self.visit(custom.identifier, indent=indent + 1)
        print('    ' * indent, 'end_CustomMode', sep='')

    def visit_DiscreteMode(self, discrete, indent=0):
        print('DiscreteMode')
        print('    ' * (indent + 1), discrete.mode, sep='')
        print('    ' * indent, 'end_DiscreteMode', sep='')

    def visit_DiscreteRangeMode(self, discreterange, indent=0):
        print('DiscreteRangeMode')
        self.visit(discreterange.mode, indent=indent + 1)
        self.visit(discreterange.bounds[0], indent=indent + 1)
        self.visit(discreterange.bounds[1], indent=indent + 1)
        print('    ' * indent, 'end_DiscreteRangeMode', sep='')

    def visit_ReferenceMode(self, reference, indent=0):
        print('ReferenceMode')
        self.visit(reference.mode, indent=indent + 1)
        print('    ' * indent, 'end_ReferenceMode', sep='')

    def visit_StringMode(self, string, indent=0):
        print('StringMode')
        self.visit(string.length, indent=indent + 1)
        print('    ' * indent, 'end_StringMode', sep='')

    def visit_ArrayMode(self, array, indent=0):
        print('ArrayMode')
        for i in array.indexes:
            self.visit(i, indent=indent + 1)
        self.visit(array.mode, indent=indent + 1)
        print('    ' * indent, 'end_ArrayMode', sep='')

    def visit_Range(self, index, indent=0):
        print('Range')
        self.visit(index.lower, indent=indent + 1)
        self.visit(index.upper, indent=indent + 1)
        print('    ' * indent, 'end_Range', sep='')

    def visit_ConditionalExpression(self, conditional, indent=0):
        print('ConditionalExpression')
        self.visit(conditional.cond, indent=indent + 1)
        self.visit(conditional.then, indent=indent + 1)
        if hasattr(conditional, 'elsif'):
            self.visit(conditional.elsif, indent=indent + 1)
        self.visit(conditional.else_clause, indent=indent + 1)
        print('    ' * indent, 'end_ConditionalExpression', sep='')

    def visit_ElsifExpression(self, elsif, indent=0):
        print('ElsifExpression')
        self.visit(elsif.cond, indent=indent + 1)
        self.visit(elsif.then, indent=indent + 1)
        if hasattr(elsif, 'elsif'):
            self.visit(elsif.elsif, indent=indent + 1)
        print('    ' * indent, 'end_ElsifExpression', sep='')

    def visit_UnaryOperation(self, operation, indent=0):
        print('UnaryOperation')
        print('    ' * indent, '  operator', sep='')
        print('    ' * (indent + 1), operation.op, sep='')
        self.visit(operation.operand, indent=indent + 1)
        print('    ' * indent, 'end_UnaryOperation', sep='')

    def visit_BinaryOperation(self, operation, indent=0):
        print('BinaryOperation')
        print('    ' * indent, '  operator', sep='')
        print('    ' * (indent + 1), operation.op, sep='')
        self.visit(operation.left, indent=indent + 1)
        self.visit(operation.right, indent=indent + 1)
        print('    ' * indent, 'end_BinaryOperation', sep='')

    def visit_Literal(self, literal, indent=0):
        print('Literal')
        print('    ' * (indent + 1), literal.mode, sep='')
        print('    ' * (indent + 1), literal.value, sep='')
        print('    ' * indent, 'end_Literal', sep='')

    def visit_ReferencedLocation(self, referenced, indent=0):
        print('ReferencedLocation')
        self.visit(referenced.loc, indent=indent + 1)
        print('    ' * indent, 'end_ReferencedLocation', sep='')

    def visit_LocationName(self, location, indent=0):
        print('LocationName')
        self.visit(location.identifier, indent=indent + 1)
        print('    ' * indent, 'end_LocationName', sep='')

    def visit_DereferencedReference(self, dereferenced, indent=0):
        print('DereferencedReference')
        self.visit(dereferenced.ref, indent=indent + 1)
        print('    ' * indent, 'end_DereferencedReference', sep='')

    def visit_IndexedElement(self, indexed, indent=0):
        print('IndexedElement')
        self.visit(indexed.identifier, indent=indent + 1)
        for i in indexed.indexes:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_IndexedElement', sep='')

    def visit_Slice(self, myslice, indent=0):
        print('Slice')
        self.visit(myslice.identifier, indent=indent + 1)
        self.visit(myslice.bounds[0], indent=indent + 1)
        self.visit(myslice.bounds[1], indent=indent + 1)
        print('    ' * indent, 'end_Slice', sep='')

    def visit_Declaration(self, declaration, indent=0):
        print('Declaration')
        for i in declaration.ids:
            self.visit(i, indent=indent + 1)
        self.visit(declaration.mode, indent=indent + 1)
        if hasattr(declaration, 'init'):
            self.visit(declaration.init, indent=indent + 1)
        print('    ' * indent, 'end_Declaration', sep='')

    def visit_Identifier(self, identifier, indent=0):
        print('Identifier')
        print('    ' * (indent + 1), identifier.identifier, sep='')
        print('    ' * indent, 'end_Identifier', sep='')

    def visit_Synonym(self, synonym, indent=0):
        print('Synonym')
        for i in synonym.ids:
            self.visit(i, indent=indent + 1)
        self.visit(synonym.expr, indent=indent + 1)
        if hasattr(synonym, 'mode'):
            self.visit(synonym.mode, indent=indent + 1)
        print('    ' * indent, 'end_Synonym', sep='')

    def visit_NewMode(self, newmode, indent=0):
        print('NewMode')
        for i in newmode.ids:
            self.visit(i, indent=indent + 1)
        self.visit(newmode.mode, indent=indent + 1)
        print('    ' * indent, 'end_NewMode', sep='')

    def visit_Procedure(self, procedure, indent=0):
        print('Procedure')
        for i in procedure.params:
            self.visit(i, indent=indent + 1)
        if hasattr(procedure, 'result'):
            self.visit(procedure.result[0], indent=indent + 1)
            if procedure.result[1] is True:
                print('    ' * (indent + 2), 'LOC', sep='')
        for i in procedure.stmts:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_Procedure', sep='')

    def visit_FormalParameter(self, formal, indent=0):
        print('FormalParameter')
        if hasattr(formal, 'loc'):
            if formal.loc is True:
                print('    ' * (indent + 1), 'LOC', sep='')
        for i in formal.ids:
            self.visit(i, indent=indent + 1)
        self.visit(formal.mode, indent=indent + 1)
        print('    ' * indent, 'end_FormalParameter', sep='')

    def visit_IfAction(self, ifaction, indent=0):
        print('IfAction')
        self.visit(ifaction.cond, indent=indent + 1)
        for i in ifaction.then:
            self.visit(i, indent=indent + 1)
        if hasattr(ifaction, 'elsif'):
            self.visit(ifaction.elsif, indent=indent + 1)
        if hasattr(ifaction, 'else_clause'):
            print('    ' * indent, 'ElseAction:', sep='')
            for i in ifaction.else_clause:
                self.visit(i, indent=indent + 1)
            print('    ' * indent, 'end_ElseAction', sep='')
        print('    ' * indent, 'end_IfAction', sep='')

    def visit_ElsifAction(self, elsifaction, indent=0):
        print('ElsifAction')
        self.visit(elsifaction.cond, indent=indent + 1)
        for i in elsifaction.then:
            self.visit(i, indent=indent + 1)
        if hasattr(elsifaction, 'elsif'):
            self.visit(elsifaction.elsif, indent=indent + 1)
        print('    ' * indent, 'end_ElsifAction', sep='')

    def visit_DoAction(self, doaction, indent=0):
        print('DoAction')
        if hasattr(doaction, 'for_cond'):
            self.visit(doaction.for_cond, indent=indent + 1)
        if hasattr(doaction, 'cond'):
            self.visit(doaction.cond, indent=indent + 1)
        for i in doaction.stmts:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_DoAction', sep='')

    def visit_StepEnumeration(self, enumeration, indent=0):
        print('StepEnumeration')
        self.visit(enumeration.counter, indent=indent + 1)
        self.visit(enumeration.start, indent=indent + 1)
        self.visit(enumeration.end, indent=indent + 1)
        if hasattr(enumeration, 'step'):
            self.visit(enumeration.step, indent=indent + 1)
        if hasattr(enumeration, 'down'):
            print('    ' * (indent + 1), enumeration.down, sep='')
        print('    ' * indent, 'end_StepEnumeration', sep='')

    def visit_RangeEnumeration(self, enumeration, indent=0):
        print('RangeEnumeration')
        self.visit(enumeration.counter, indent=indent + 1)
        self.visit(enumeration.mode, indent=indent + 1)
        if hasattr(enumeration, 'down'):
            print('    ' * (indent + 1), enumeration.down, sep='')
        print('    ' * indent, 'end_RangeEnumeration', sep='')

    def visit_AssignmentAction(self, action, indent=0):
        print('AssignmentAction')
        self.visit(action.loc, indent=indent + 1)
        self.visit(action.expr, indent=indent + 1)
        print('    ' * indent, 'end_AssignmentAction', sep='')

    def visit_CallAction(self, action, indent=0):
        print('CallAction')
        self.visit(action.proc, indent=indent + 1)
        for i in action.params:
            self.visit(i, indent=indent + 1)
        print('    ' * indent, 'end_CallAction', sep='')

    def visit_ExitAction(self, action, indent=0):
        print('ExitAction')
        self.visit(action.label, indent=indent + 1)
        print('    ' * indent, 'end_ExitAction', sep='')

    def visit_ReturnAction(self, action, indent=0):
        print('ReturnAction')
        if hasattr(action, 'result'):
            self.visit(action.result, indent=indent + 1)
        print('    ' * indent, 'end_ReturnAction', sep='')

    def visit_ResultAction(self, action, indent=0):
        print('ResultAction')
        self.visit(action.result, indent=indent + 1)
        print('    ' * indent, 'end_ResultAction', sep='')
