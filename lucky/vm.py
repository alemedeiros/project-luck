# vm.py
# Project 3 - MC911
# luck - Lya Ultra Compiler Kit
#
# Virtual Machine Implementation
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Virtual Machine related code.
"""
import pprint
import sys

class VirtualMachine(object):
    def __init__(self, h, code):
#        print('iniciando virtual machine', file=sys.stderr)
        self.h = h
        self.code = code

        # Compute labels addresses
        self.labels = {}
        for i, inst in enumerate(self.code):
            if inst[0] == 'lbl':
                self.labels[inst[1]] = i

    def run(self):
        # Initialize virtual machine variables
#        print('metodo run', file=sys.stderr)
        self.pc = 0
        self.sp = 0
        self.M = [None]*1000
        self.D = [None]*1000

        print("\n Jump table", file=sys.stderr)
        pprint.pprint(self.labels, stream=sys.stderr)
        print("\n")


        # Run instructions from address 0, until end instruction is reached
        while self.code[self.pc][0] != 'end':
#            input()
            self.run_instruction(self.code[self.pc])

    def run_instruction(self, inst):
        # DEBUG
#        print('running', inst[0], file=sys.stderr)
        # Properly call specific method for running the instruction.
        method = 'run_' + inst[0]
        run = getattr(self, method)
        run(inst)
#        print('m:', self.M[:self.sp+1], file=sys.stderr)
#        print('pc:', self.pc, file=sys.stderr)
#        print('D:',self.D[:self.sp+1], file=sys.stderr)

    def run_lbl(self, inst):
        # When running code, lbl is a nop
        self.pc += 1

    def run_ldc (self, inst):
        self.sp += 1
        self.M[self.sp]=inst[1]
        self.pc += 1

    def run_ldv (self, inst):
        self.sp += 1
        self.M[self.sp]=self.M[self.D[inst[1]]+inst[2]]
        self.pc += 1

    def run_ldr (self, inst):
        self.sp += 1
        self.M[self.sp]=self.D[inst[1]]+inst[2]
        self.pc += 1

    def run_stv (self, inst):
        self.M[self.D[inst[1]]+inst[2]]=self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_lrv (self, inst):
        self.sp += 1
        self.M[self.sp]=self.M[self.M[self.D[inst[1]]+inst[2]]]
        self.pc += 1

    def run_srv (self, inst):
        self.M[self.M[self.D[inst[1]]+inst[2]]]=self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_add (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] + self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_sub (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] - self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_mul (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] * self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_div (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] // self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_mod (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] % self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_neg (self, inst):
        self.M[self.sp]= -self.M[self.sp]
        self.pc += 1

    def run_and (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] and self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_lor (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] or self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_not (self, inst):
        self.M[self.sp]=not self.M[self.sp]
        self.pc += 1

    def run_les (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] < self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_leq (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] <= self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_grt (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] > self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_gre (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] >= self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_equ (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] == self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_neq (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] != self.M[self.sp]
        self.sp -= 1
        self.pc += 1

    def run_jmp (self, inst):
        self.pc = self.labels[inst[1]]

    def run_jof (self, inst):
        if not self.M[self.sp]:
            self.pc=self.labels[inst[1]]
        else:
            self.pc += 1
        self.sp -= 1

    def run_alc (self, inst):
        self.sp += inst[1]
        self.pc += 1

    def run_dlc (self, inst):
        self.sp -= inst[1]
        self.pc += 1

    def run_cfu (self, inst):
        self.sp += 1
        self.M[self.sp]=self.pc+1
        self.pc = self.labels[inst[1]]

    def run_enf (self, inst):
        self.sp += 1
        self.M[self.sp]=self.D[inst[1]]
        self.D[inst[1]]=self.sp+1
        self.pc += 1

    def run_ret (self, inst):
        self.D[inst[1]]=self.M[self.sp]
        self.pc=self.M[self.sp-1]
        n = inst[2]+2
        self.sp -= n

    def run_idx (self, inst):
        self.M[self.sp-1]=self.M[self.sp-1] + self.M[self.sp] * inst[1]
        self.sp -= 1
        self.pc += 1

    def run_grc (self, inst):
        self.M[self.sp]=self.M[self.M[self.sp]]
        self.pc += 1

    def run_lmv (self, inst):
        t=self.M[self.sp]
        self.M[self.sp:self.sp+inst[1]]=self.M[t:t+inst[1]]
        n = inst[1]-1
        self.sp += n
        self.pc += 1

    def run_smv (self, inst):
        t = self.M[self.sp-inst[1]]
        self.M[t:t+inst[1]] =self.M[self.sp-inst[1]+1:self.sp+1]
        n = inst[1]+1
        self.sp -= n
        self.pc += 1

    def run_smr (self, inst):
        t1 = self.M[self.sp-1]
        t2 = self.M[self.sp]
        self.M[t1:t1+inst[1]] = self.M[t2:t2+inst[1]]
        self.sp -= 1
        self.pc += 1

    def run_sts (self, inst):
        adr=self.M[self.sp]
        self.M[adr]=len(self.h[inst[1]])
        for c in self.h[inst[1]]:
            adr += 1
            self.M[adr]=c;
        self.sp -= 1
        self.pc += 1

    def run_rdv (self, inst):
        self.sp += 1
        self.M[self.sp]=(eval(input()))
        self.pc += 1

    def run_rds (self, inst):
        string=input()
        adr=self.M[self.sp]
        self.M[adr] = len(string)
        for k in string:
            adr += 1
            self.M[adr]=k
        self.sp -= 1
        self.pc += 1

    def run_prv (self, inst):
        print(self.M[self.sp])
        self.sp-=1
        self.pc+=1

    def run_prt (self, inst):
        print(self.M[self.sp-inst[1]+1:self.sp+1])
        n = inst[1]-1
        self.sp -= n
        self.pc+=1

    def run_prc (self, inst):
        print(self.h[inst[1]],end="")
        self.pc += 1

    def run_prs (self, inst):
        adr_aux = self.M[self.sp]
        len_aux = self.M[adr_aux]
        for i in range (0, len_aux):
            adr_aux+=1
            print(self.M(adr_aux),end="")
        self.sp -= 1
        self.pc += 1

    def run_stp (self, inst):
        self.sp = -1
        self.D[0] = 0;
        self.pc += 1

