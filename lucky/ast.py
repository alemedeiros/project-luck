# ast.py
# Project 1 - MC911
# luck - Lya Ultra Compiler Kit
#
# Lexical & Syntactic Analysis of Lya language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Lucky Abstract Syntax Tree data structure description.
"""

import sys


# Node identifier counter
node_id = 0


# AST node base class
# TODO(alemedeiros): a nice feature would be to generate a .dot file from AST
class AST(object):
    """
    Base class example for the AST nodes.  Each node is expected to define the
    _fields attribute which lists the names of stored attributes.  The
    __init__() method declared in this class takes positional arguments and
    assigns them to the appropriate fields (in the same order).  Any additional
    arguments specified as keywords are also assigned.
    """
    _fields = []
    _opt_fields = []

    def __init__(self, *args, **kwargs):
        assert len(args) == len(self._fields)
        for name, value in zip(self._fields, args):
            setattr(self, name, value)
        # Assign additional keyword arguments if supplied
        for name, value in kwargs.items():
            if name not in self._opt_fields:
                print(self.__class__.__name__, ': unexpected optional field ',
                      name, sep='', file=sys.stderr)
            setattr(self, name, value)
        global node_id
        setattr(self, 'nid', node_id)
        node_id += 1


# -- AST root node --
class Program(AST):
    """
    This is the root of the AST and contains the complete program.

    Fields:
        - stmts: A list of Statement nodes.
    """
    _fields = ['stmts']


# -- Statement nodes --
class Statement(AST):
    """
    General Statement node (Abstract Class).

    Fields: none
    """
    # TODO(alemedeiros): think if there is any common field for all Statements
    _fields = []


class DeclarationStatement(Statement):
    """
    Statement subclass to encapsulate variable declarations.

    Fields:
        - dcls: A list of Declaration nodes (can not be empty).
    """
    _fields = Statement._fields + ['dcls']


class SynonymStatement(Statement):
    """
    Statement subclass to encapsulate synonyms declarations.

    Fields:
        - syns: A list of Synonym nodes (can not be empty).
    """
    _fields = Statement._fields + ['syns']


class NewModeStatement(Statement):
    """
    Statement subclass to encapsulate new mode declarations.

    Fields:
        - newmodes: A list of NewMode nodes (can not be empty).
    """
    _fields = Statement._fields + ['newmodes']


class ProcedureStatement(Statement):
    """
    Statement subclass to encapsulate procedure declarations.

    Fields:
        - label: The identifier of the procedure (Identifier node).
        - definition: Definition of the procedure (Procedure node).
    """
    _fields = Statement._fields + ['label', 'definition']


class ActionStatement(Statement):
    """
    Statement subclass to encapsulate action statements.

    Fields:
        - action: Specify the type of action (action node)
        * label: Optional label for the statement (Identifier).
    """
    _fields = Statement._fields + ['action']
    _opt_fields = Statement._opt_fields + ['label']


# -- Mode related nodes --
class Mode(AST):
    """
    Node used to represent a general Mode (Abstract class).

    Fields: none
    """
    # TODO(alemedeiros): think if there is any common field for all Modes
    _fields = []


class CustomMode(Mode):
    """
    User defined mode.

    Fields:
        - identifier: Name of the mode (Identifier node).
    """
    _fields = Mode._fields + ['identifier']


class DiscreteMode(Mode):
    """
    Mode used for discrete variable types.

    Fields:
        - mode: The mode itself (Enumeration: int, bool, char)
    """
    _fields = Mode._fields + ['mode']


class DiscreteRangeMode(Mode):
    """
    Mode consisting of a range of a Discrete Mode.

    Fields:
        - mode: Mode of the range -- Must be either a (discrete) CustomMode or
            a DiscreteMode (Mode node).
        - bounds: A pair describing the range, where the first element is the
            lower bound and the second the upper bound (Pair of Expression
            nodes).
    """
    _fields = Mode._fields + ['mode', 'bounds']


class ReferenceMode(Mode):
    """
    Mode representing a reference to another mode.

    Fields:
        - mode: Referenced Mode (Mode node).
    """
    _fields = Mode._fields + ['mode']


class StringMode(Mode):
    """
    Mode for representing Strings.

    Fields:
        - length: Length of the declared string -- must be an Integer (Literal
            node).
    """
    _fields = Mode._fields + ['length']


class ArrayMode(Mode):
    """
    Mode for representing Arrays.

    Fields:
        - indexes: A list of indexes, each index may be either a DiscreteMode
            node or a Range node.
        - mode: Mode of the elements on the Array (Mode node).
    """
    _fields = Mode._fields + ['indexes', 'mode']


class Range(AST):
    """
    A pair of Expressions defining a range.

    Fields:
        - lower: The lower bound of the range (Expression node).
        - upper: The upper bound of the range (Expression node).
    """
    _fields = ['lower', 'upper']


# -- Expression related nodes --
class Expression(AST):
    """
    Node used to represent a General Expressions (Abstract class).

    Fields: none
    """
    # TODO(alemedeiros): think if there is any common field for all Expressions
    _fields = []


class ConditionalExpression(Expression):
    """
    Conditional expression, depending on the value of the condition, the then
    or the else expressions should be used, if there is an elsif clause, it
    will be evalueted before the else clause.

    Fields:
        - cond: Condition which will be used to decide between then or else
            clause -- Must be a boolean expression (Expression node).
        - then: Expression used when the condition is True (Expression node).
        - else_clause: Expression used when the condition is False (Expression
            node).
        * elsif: Optional else-if construct (ElsifExpression node).
    """
    _fields = Expression._fields + ['cond', 'then', 'else_clause']
    _opt_fields = Expression._opt_fields + ['elsif']


class ElsifExpression(AST):
    """
    Complementary Elsif Expression for Conditional Expressions.

    Fields:
        - cond: Condition which will be used to decide whether to follow the
            then clause -- Must be a boolean expression (Expression node).
        - then: Expression used when the condition is True (Expression).
        * elsif: Optional Else-if construct (ElsifExpression node).
    """
    _fields = ['cond', 'then']
    _opt_fields = ['elsif']


class UnaryOperation(Expression):
    """
    A Unary Operation.

    Fields:
        - op: Operator of the operation (Enumeration: MINUS, NOT)
        - operand: Operand (Expression)
    """
    _fields = Expression._fields + ['op', 'operand']


class BinaryOperation(Expression):
    """
    A Binary Operation.

    Fields:
        - op: Operator of the Binary operation (Enumeration: AND, OR, EQ, NEQ,
            GT, GET, LT, LEQ, IN, PLUS, MINUS, TIMES, DIVIDE, MOD).
        - left: Left Operand (Expression)
        - right: Right Operand (Expression)
    """
    _fields = Expression._fields + ['op', 'left', 'right']


class Literal(Expression):
    """
    Literal Value.

    Fields:
        - mode: The mode of the literal -- must be either an integer, boolean,
            character, string or a null mode (Mode node).
        - value: Value of the literal.
    """
    _fields = Expression._fields + ['mode', 'value']


# -- Location related nodes --
class ReferencedLocation(Expression):
    """
    Node used to represent a reference to a location.

    Fields:
        - loc: Location being referenced (Location node).
    """
    _fields = Expression._fields + ['loc']


class Location(Expression):
    """
    Node used to represent a General Location (Abstract class).

    Fields: none
    """
    # TODO(alemedeiros): think if there is any common field for all Locations
    _fields = Expression._fields + []


class LocationName(Location):
    """
    A location represented by a variable.

    Fields:
        - identifier: Name of the variable (Identifier node).
    """
    _fields = Location._fields + ['identifier']


class DereferencedReference(Location):
    """
    A Location consisting of a reference which as dereferenced.

    Fields:
        - ref: The reference which was dereferenced (Location node)
    """
    _fields = Location._fields + ['ref']


class IndexedElement(Location):
    """
    An indexed element from an array-like type (Array or String).

    Fields:
        - identifier: Identifier of the array-like variable (Identifier node).
        - indexes: The list of indexes of the desired element -- if we're
            dealing with a String, the list **must** have only one element
            (List of Expression nodes).
    """
    # TODO(any): Check whether Strings are really always one dimensional
    _fields = Location._fields + ['identifier', 'indexes']


class Slice(Location):
    """
    An slice of an array-like type (Array or String).

    Fields:
        - identifier: Identifier of the array-like variable (Identifier node).
        - bounds: A pair describing the range, where the first element is the
            lower bound and the second the upper bound (Pair of Expression
            nodes).
    """
    _fields = Location._fields + ['identifier', 'bounds']


# -- Declaration related nodes --
class Declaration(AST):
    """
    Variable declaration related node.

    Fields:
        - ids: A list of Identifier nodes.
        - mode: The mode of the declaration (Mode node).
        * init: Optional initialization of the declaration (Expression node).
    """
    _fields = ['ids', 'mode']
    _opt_fields = ['init']


class Identifier(AST):
    """
    Identifier, the name which may be used to refer to multiple objects on the
    language(variables, methods, synonyms, modes etc).

    Fields:
        - identifier: name used for the identifier (String).
        * lineno: Optional line where identifier was found (Integer).
    """
    _fields = ['identifier']


# -- Synonym related nodes --
class Synonym(AST):
    """
    Constant declaration node, used to associate identifiers with cosntant
    values.

    Fields:
        - ids: A list of Identifier nodes.
        - expr: The expression describing the synonym -- Must be constant
            (Expression node).
        * mode: Optional mode of the synonym declaration (Mode node).
    """
    _fields = ['ids', 'expr']
    _opt_fields = ['mode']


# -- New Mode related nodes --
class NewMode(AST):
    """
    Custom mode declaration node.

    Fields:
        - ids: A list of Identifier nodes.
        - mode: Mode representation of the custom mode declaration (Mode node).
    """
    _fields = ['ids', 'mode']


# -- Procedure related nodes --
class Procedure(AST):
    """
    The definition of a Procedure.

    Fields:
        - params: Parameters definition (List of FormalParameter node).
        - stmts: The list of Statement of this procedure.
        * result: Specification of the procedure return value. A pair of Mode
            node and a boolean representing whether the parameter is a LOC or
            not.
    """
    _fields = ['params', 'stmts']
    _opt_fields = ['result']


class FormalParameter(AST):
    """
    Procedure parameter definition.

    Fields:
        - ids: A list of Idenfiers.
        - mode: The mode of the parameters (Mode node).
        * loc: whether the parameter is a LOC or not (Boolean value).
    """
    # NOTE: A LOC parameter behaves similarly to a C++ '&' reference.
    _fields = ['ids', 'mode']
    _opt_fields = ['loc']


# -- Action related nodes --
class Action(AST):
    """
    Node used to represent a General Action (Abstract class).

    Fields: none
    """
    # TODO(alemedeiros): think if there is any common field for all Actions
    _fields = []


class IfAction(Action):
    """
    Action representing an if on the programing language. Else is optional.

    Fields:
        - cond: Condition which will be used to decide between then or else
            statements -- Must be a boolean expression (Expression node).
        - then: Statements to run when the condition is True (List of
            ActionStatement nodes).
        * else_clause: Optional Statements to run when the condition is False
            (List of ActionStatement nodes).
        * elsif: Optional else-if construct (ElsifAction node).
    """
    _fields = Action._fields + ['cond', 'then']
    _opt_fields = Action._opt_fields + ['else_clause', 'elsif']


class ElsifAction(AST):
    """
    Complementary Elsif Action for IfActions.

    Fields:
        - cond: Condition which will be used to decide whether to run the then
            statements -- Must be a boolean expression (Expression node).
        - then: Expression used when the condition is True. (List of
            ActionStatement nodes).
        * elsif: Optional Else-if construct (ElsifAction node).
    """
    _fields = ['cond', 'then']
    _opt_fields = ['elsif']


class DoAction(Action):
    """
    Action representing Loops, may be a for-loop or a while-loop.

    Fields:
        - stmts: Statements nested on this loop (List of ActionStatement
            nodes).
        * for_cond: control of a for-loop (ForControl node).
        * cond: Condition for the loop -- must be a boolean expression
            (Expression node).
    """
    _fields = Action._fields + ['stmts']
    _opt_fields = Action._opt_fields + ['for_cond', 'cond']


class ForControl(AST):
    """
    Control of a for-loop (Abstract class).

    Fields:
        - counter: Loop counter variable name (Identifier node).
        * down: Optional value to indicate that the step is downwards (Boolean
            value).
    """
    _fields = ['counter']
    _opt_fields = ['down']


class StepEnumeration(ForControl):
    """
    Step enumeration option of a for-loop control.

    Fields:
        - start: Counter start value -- must be a discrete mode (Expression
            node).
        - end: Counter end value -- must be a discrete mode (Expression node).
        * step: Optional alternative step value -- must be an integer mode
            (Expression node).
    """
    _fields = ForControl._fields + ['start', 'end']
    _opt_fields = ForControl._opt_fields + ['step']


class RangeEnumeration(ForControl):
    """
    Range enumeration option of a for-loop control.

    Fields:
        - mode: mode of the counter variable -- must be a discrete range (Mode
            node).
    """
    _fields = ForControl._fields + ['mode']


class AssignmentAction(Action):
    """
    Action representing variable assignments.

    Fields:
        - loc: Location into which the assignment is being made (Location
            node).
        - expr: Expression being assined to the location (Expression node).
    """
    # NOTE: optional operator is a syntax-sugar that should be dealt with at
    # parsing time.
    _fields = Action._fields + ['loc', 'expr']


class CallAction(Action, Location):
    """
    Action of calling a procedure, the result may be a location.

    Fields:
        - proc: Procedure name, may be an user-defined or a builtin procedure
            (Identifier node).
        - params: Parameter list of the call (List of Expression nodes).
    """
    _fields = Action._fields + Location._fields + ['proc', 'params']


class ExitAction(Action):
    """
    Exit Action.

    Fields:
        - label: Identifier of the label (Identifier node).
    """
    # TODO(any): find out what this action means, semantically
    _fields = Action._fields + ['label']


class ReturnAction(Action):
    """
    Action of returning from a procedure, optionally setting the return value
    of the procedure.

    Fields:
        * result: Optional return value (Expression node)
    """
    _fields = Action._fields + []
    _opt_fields = Action._opt_fields + ['result']


class ResultAction(Action):
    """
    Action of setting the value of the return value of the current procedure.

    Fields:
        - result: value to be assigned to the return value (Expression node).
    """
    _fields = Action._fields + ['result']
