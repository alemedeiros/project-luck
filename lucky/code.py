# code.py
# Project 2 - MC911
# luck - Lya Ultra Compiler Kit
#
# Semantic & Code Generation of Lya Language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Code Generation related code.
"""

from . import ast
from . import semantic
from . import visitors


class CodeGenEnv(object):
    def __init__(self):
        self.offset_stack = []
        self.sym_stack = []
        self.offset_levels = []

    def push(self, offsets, syms, vars_offset, args_offset=0):
        self.offset_stack.append(offsets)
        self.sym_stack.append(syms)
        self.offset_levels.append((vars_offset, args_offset))

    def pop(self):
        self.offset_stack.pop()
        self.sym_stack.pop()
        self.offset_levels.pop()

    def lookup_sym(self, name):
        for scope in reversed(self.sym_stack):
            hit = scope.get(name, None)
            if hit is not None:
                return hit
        return None

    def lookup_offset(self, name):
        level = self.level()
        for scope in reversed(self.offset_stack):
            hit = scope.get(name, None)
            if hit is not None:
                return (level - 1, hit)
            level -= 1

        return None

    def last_vars_offset(self):
        return self.offset_levels[-1][0]

    def level(self):
        return len(self.offset_stack)


class CodeGen(visitors.NodeVisitor):

    def __init__(self):
        self.code = []
        self.label_id = 0
        self.proc_labels = {}
        self.env = CodeGenEnv()
        self.h = []

    def add_code(self, c):
        if isinstance(c, list):
            self.code += c
        else:
            self.code.append(c)

    def visit_Program(self, node, **kwargs):
        # Get last label used for procedures
        self.label_id = node.last_label

        self.add_code(('stp',))  # Start Program instruction

        # Allocate space for variables on stack and push data to environment
        self.add_code(('alc', node.vars_offset))
        self.env.push(node.offsets, node.sym_table, node.vars_offset)

        # Visit Statements
        for stmt in node.stmts:
            self.visit(stmt, **kwargs)

        # Deallocate space for variables from stack
        self.add_code(('dlc', node.vars_offset))
        self.env.pop()

        self.add_code(('end',))  # End Program instruction

        self.h = node.h

    def visit_ProcedureStatement(self, node, **kwargs):
        # if function is declared in the middle of some statements, make a jump
        # around it
        wrap_id = self.label_id
        exit_id = self.label_id + 1
        self.label_id += 2
        self.add_code(('jmp', wrap_id))

        # Save procedure label
        self.add_code(('lbl', node.proc_label))
        self.proc_labels[node.label.identifier] = node.proc_label

        # Enter function and Allocate space for local variables on stack and
        # push data to environment
        self.env.push(node.offsets, node.sym_table, node.vars_offset,
                      node.args_offset)
        level = self.env.level() - 1
        self.add_code(('enf', level))
        if node.vars_offset > 0:
            self.add_code(('alc', node.vars_offset))

        # Procedure code
        tmp = None
        if '_exit' in kwargs:
            tmp = kwargs.pop('_exit')
        for stmt in node.definition.stmts:

            self.visit(stmt, _exit=exit_id, **kwargs)
        if tmp is not None:
            kwargs['_exit'] = tmp

        # Return from procedure
        self.add_code(('lbl', exit_id))
        if node.vars_offset > 0:
            self.add_code(('dlc', node.vars_offset))
        self.add_code(('ret', level, node.args_offset))
        self.env.pop()

        # Make label so procedure code is skipped unless called
        self.add_code(('lbl', wrap_id))

    def builtin_code(self, node, value=False):
        if node.proc.identifier == 'num':
            pass  # no idea how to implement this with LVM instructions
        elif node.proc.identifier == 'pred':  # Predecessor
            self.visit(node.params[0], value=True)
            self.add_code(('ldc', 1))
            self.add_mode(('sub',))

            if not value:
                # Deallocate unused return value
                self.add_code(('dlc', 1))
        elif node.proc.identifier == 'succ':  # Successor
            self.visit(node.params[0], value=True)
            self.add_code(('ldc', 1))
            self.add_mode(('add',))

            if not value:
                # Deallocate unused return value
                self.add_code(('dlc', 1))
        elif node.proc.identifier == 'upper':
            _t = node.params[0].sem_type  # MUST be an ArrayType
            upper = _t.indexes[0][1]
            self.add_code(('ldc', upper))

            if not value:
                # Deallocate unused return value
                self.add_code(('dlc', 1))
        elif node.proc.identifier == 'lower':
            _t = node.params[0].sem_type  # MUST be an ArrayType
            lower = _t.indexes[0][0]
            self.add_code(('ldc', lower))

            if not value:
                # Deallocate unused return value
                self.add_code(('dlc', 1))
        elif node.proc.identifier == 'length':
            _t = node.params[0].sem_type  # MUST be an StringType
            self.add_code(('ldc', _t.length))

            if not value:
                # Deallocate unused return value
                self.add_code(('dlc', 1))
        elif node.proc.identifier == 'read':
            for _p in node.params:
                if isinstance(_p.sem_type, semantic.ArrayType):
                    n = 1
                    for lower, upper in _p.sem_type.indexes:
                        n *= upper - lower + 1

                    # Get array address
                    # Assuming read will only receive variables, otherwise, the
                    # laguage would make even less sense
                    i, j = self.env.lookup_offset(_p.identifier.identifier)
                    self.add_code(('ldr', i, j))

                    # Read n values
                    self.add_code([('rdv',) for _ in range(n)])

                    # Store the values
                    self.add_code(('smv', n))
                elif isinstance(_p.sem_type, semantic.StringType):
                    # Get string address
                    # Assuming read will only receive variables, otherwise, the
                    # laguage would make even less sense
                    i, j = self.env.lookup_offset(_p.identifier.identifier)
                    self.add_code(('ldr', i, j))
                    self.add_code(('rds',))
                else:
                    indexed = False
                    if isinstance(_p, ast.IndexedElement):
                        self.visit(_p, store=True)
                        indexed = True
                    # Parameter must be a value
                    self.add_code(('rdv',))
                    if indexed:
                        self.add_code(('smv', 1))
                    else:
                        self.visit(_p, store=True)
        elif node.proc.identifier == 'print':
            for _p in node.params:
                if isinstance(_p.sem_type, semantic.ArrayType):
                    n = 1
                    for lower, upper in _p.sem_type.indexes:
                        n *= upper - lower + 1

                    # Load array and call print multiple values
                    self.visit(_p, value=True)
                    self.add_code(('prt', n))
                elif isinstance(_p.sem_type, semantic.StringType):
                    if isinstance(_p, ast.Literal):
                        self.add_code(('prc', _p.index))
                    else:
                        # Get string address
                        # Assuming read will only receive variables, otherwise,
                        # the laguage would make even less sense
                        i, j = self.env.lookup_offset(_p.identifier.identifier)
                        self.add_code(('ldr', i, j))
                        self.add_code(('prs',))
                else:
                    # Parameter must be a value
                    self.visit(_p, value=True)
                    self.add_code(('prv',))
        else:
            pass  # Should not have come to this...

    def offset_size(self, t):
        # TODO: check for loc?
        if isinstance(t, (semantic.IntType, semantic.CharType,
                          semantic.BoolType, semantic.ReferenceType)):
            return 1
        elif isinstance(t, semantic.StringType):
            return t.length + 1
        elif isinstance(t, semantic.ArrayType):
            array_size = 1
            for lower, upper in t.indexes:
                array_size = (upper - lower + 1) * array_size
            return array_size
        return 0

    def visit_CallAction(self, node, store=False, value=False, **kwargs):
        builtins = ['num', 'pred', 'succ', 'upper', 'lower', 'length', 'read',
                    'print']
        if node.proc.identifier in builtins:
            # Builtin call
            self.builtin_code(node, value=value)
        else:  # Procedure call
            _p = self.env.lookup_sym(node.proc.identifier)
            ret = False

            # Check for possible return value
            if _p['return'] != semantic.NoType():
                sz = self.offset_size(_p['return'])
                if sz > 0:
                    self.add_code(('alc', sz))
                ret = True

            # Create code to put parameters on the stack
            for i in node.params:
                if i.loc:
                    _i, _j = self.env.lookup_offset(i.identifier.identifier)
                    self.add_code(('ldr', _i, _j))
                else:
                    self.visit(i, value=True, **kwargs)

            # Effectivelly calls procedure
            self.add_code(('cfu', _p['label']))

            if ret:
                if store:  # Then the return was a loc
                    # return must be an reference or a loc
                    # Don't need to do anything, as address will be on top of
                    # stack
                    pass
                elif not value:
                    # Deallocate unused return value
                    if sz > 0:
                        self.add_code(('dlc', sz))

                # Otherwise, return value is used elsewhere, don't need to
                # deallocate

    def visit_AssignmentAction(self, node, **kwargs):
        # if location of assignment is an indexed element, the order of
        # evaluation should be different, because of the instruction set
        indexed = False
        if isinstance(node.loc, ast.IndexedElement):
            self.visit(node.loc, store=True, **kwargs)
            indexed = True
        if isinstance(node.loc, ast.CallAction):
            self.visit(node.loc, store=True, **kwargs)
            indexed = True

        # Will generate code to leave the value at stack
        if hasattr(node.expr, 'value'):
            self.visit_Literal(node.expr, **kwargs)
        else:
            self.visit(node.expr, value=True, **kwargs)

        # Will store value to location
        if indexed:
            self.add_code(('smv', 1))
        else:
            self.visit(node.loc, store=True, **kwargs)

    def visit_ResultAction(self, node, **kwargs):
        # Recursively generate code for expression (assuming the result will be
        # on the top of the stack
        if node.loc:
            _i, _j = self.env.lookup_offset(node.result.identifier.identifier)
            self.add_code(('ldr', _i, _j))
        else:
            self.visit(node.result, value=True, **kwargs)

        # Copy expression value to return address
        i, j = self.env.lookup_offset('__result__')
        self.add_code(('stv', i, j))

    def visit_ReturnAction(self, node, _exit=-1, **kwargs):
        if hasattr(node, 'result'):
            self.visit_ResultAction(node, **kwargs)

        self.add_code(('jmp', _exit))

    def visit_LocationName(self, node, store=False, **kwargs):
        if hasattr(node, 'value'):
            self.visit_Literal(node, **kwargs)
            return

        i, j = self.env.lookup_offset(node.identifier.identifier)
        if store:  # Should store value
            if node.loc:
                self.add_code(('srv', i, j))
            else:
                self.add_code(('stv', i, j))
        else:  # Should read value
            if node.loc:
                self.add_code(('lrv', i, j))
            else:
                self.add_code(('ldv', i, j))

    def visit_DereferencedReference(self, node, store=False, **kwargs):
        i, j = self.env.lookup_offset(node.identifier.identifier)
        if store:  # Should store value
            self.add_code(('srv', i, j))
        else:  # Should read value
            self.add_code(('lrv', i, j))

    def visit_IndexedElement(self, node, store=False, **kwargs):
        i, j = self.env.lookup_offset(node.identifier.identifier)
        if node.loc:
            self.add_code(('ldv', i, j))
        else:
            self.add_code(('ldr', i, j))

        # Generate code to compute index expression
        _value = kwargs.pop('value', False)
        _store = kwargs.pop('store', False)
        self.visit(node.index_expr, value=True, store=False, **kwargs)
        kwargs['value'] = _value
        kwargs['store'] = _store

        # TODO: put array element size on idx instruction
        self.add_code(('idx', 1))

        if not store:
            self.add_code(('grc', ))

    def visit_Slice(self, node, store=False, **kwargs):
        i, j = self.env.lookup_offset(node.identifier.identifier)
        lower = node.bounds.lower.value
        upper = node.bounds.upper.value
        _c = [('ldr', i, j + lower)]
        if store:
            _c.append(('smv', upper - lower + 1))
            # _c = [('stv', i, j + k) for k in range(upper, lower-1, -1)]
        else:
            _c.append(('lmv', upper - lower + 1))
            # _c = [('ldv', i, j + k) for k in range(lower, upper+1)]
        self.add_code(_c)

    def visit_Literal(self, node, **kwargs):
        self.add_code(('ldc', node.value))

    def visit_ConditionalExpression(self, node, **kwargs):
        pass  # TODO

    def visit_UnaryOperation(self, node, **kwargs):
        if hasattr(node, 'value'):
            self.visit_Literal(node, **kwargs)
            return

        self.visit(node.operand, **kwargs)

        # Select instruction according to operator
        if node.op == '-':
            self.add_code(('neg',))
        elif node.op == '!':
            self.add_code(('not',))

    def visit_BinaryOperation(self, node, **kwargs):
        if hasattr(node, 'value'):
            self.visit_Literal(node, **kwargs)
            return

        self.visit(node.left, **kwargs)
        self.visit(node.right, **kwargs)

        # Select instruction according to operator
        if node.op == '+':
            self.add_code(('add',))
        elif node.op == '-':
            self.add_code(('sub',))
        elif node.op == '*':
            self.add_code(('mul',))
        elif node.op == '/':
            self.add_code(('div',))
        elif node.op == '%':
            self.add_code(('mod',))
        elif node.op == '&&':
            self.add_code(('and',))
        elif node.op == '||':
            self.add_code(('lor',))
        elif node.op == '<':
            self.add_code(('les',))
        elif node.op == '<=':
            self.add_code(('leq',))
        elif node.op == '>':
            self.add_code(('grt',))
        elif node.op == '>=':
            self.add_code(('gre',))
        elif node.op == '==':
            self.add_code(('equ',))
        elif node.op == '!=':
            self.add_code(('neq',))
        # TODO String concatenation ?

    def visit_ReferencedLocation(self, node, **kwargs):
        if hasattr(node, 'value'):
            self.visit_Literal(node, **kwargs)
            return

        self.visit(node.loc, value=True, **kwargs)
        self.add_code(('grc',))

    def visit_IfAction(self, node, **kwargs):
        # Generate code to compute condition
        self.visit(node.cond, value=True, **kwargs)

        # Determine labels
        end_label = self.label_id
        self.label_id += 1
        if hasattr(node, 'else_clause'):
            else_label = self.label_id
            self.label_id += 1
        else:
            else_label = end_label
        if hasattr(node, 'elsif'):
            elsif_label = self.label_id
            self.label_id += 1
        else:
            elsif_label = else_label

        # Create jump to elsif_label (if there is no elsif, it may be else, if
        # there is no else either, it will be end_label)
        self.add_code(('jof', elsif_label))

        # Add statements code and jump out of then
        for stmt in node.then:
            self.visit(stmt, **kwargs)
        self.add_code(('jmp', end_label))

        # Elsif code
        if hasattr(node, 'elsif'):
            self.add_code(('lbl', elsif_label))
            self.visit(node.elsif, _else=else_label, _end=end_label, **kwargs)

        # Else code
        if hasattr(node, 'else_clause'):
            self.add_code(('lbl', else_label))
            for stmt in node.else_clause:
                self.visit(stmt, **kwargs)

        # Exit if label
        self.add_code(('lbl', end_label))

    def visit_ElsifAction(self, node, _else=None, _end=None, **kwargs):
        # Generate code to compute condition
        self.visit(node.cond, value=True, **kwargs)

        # Determine labels
        if hasattr(node, 'elsif'):
            elsif_label = self.label_id
            self.label_id += 1
        else:
            elsif_label = _else

        self.add_code(('jof', elsif_label))

        # Add statements code and jump out of then
        for stmt in node.then:
            self.visit(stmt, **kwargs)
        self.add_code(('jmp', _end))

        # In case of another elsif
        if hasattr(node, 'elsif'):
            self.add_code(('lbl', elsif_label))
            self.visit(node.elsif, _else=_else, **kwargs)

    def visit_DoAction(self, node, **kwargs):
        # Initializa for variable
        if hasattr(node, 'for_cond'):
            self.visit(node.for_cond, **kwargs)

        # Determine labels
        cond_label = self.label_id
        end_label = self.label_id + 1
        self.label_id += 2

        self.add_code(('lbl', cond_label))

        # Compute while condition
        if hasattr(node, 'cond'):
            self.visit(node.cond, value=True, **kwargs)
            # When condition isn't met, jump out of loop
            self.add_code(('jof', end_label))

        # Statements
        for stmt in node.stmts:
            self.visit(stmt, **kwargs)

        # For increment and verification
        if hasattr(node, 'for_cond'):
            # The increment code will leave the result of whether the counter
            # is less than the end of the range on the top of the stack
            self.visit(node.for_cond, inc=True, **kwargs)
            self.add_code(('jof', end_label))

        # Jump to cond
        self.add_code(('jmp', cond_label))

        # Exit while label
        self.add_code(('lbl', end_label))

    def visit_StepEnumeration(self, node, inc=False, **kwargs):
        i, j = self.env.lookup_offset(node.counter.identifier)
        if inc:  # Code for step increment
            step = 1
            if hasattr(node, 'step'):
                step = node.step
                if hasattr(step, 'value'):
                    step = step.value

            # Load values for counter and step
            self.add_code(('ldv', i, j))
            if isinstance(step, ast.AST):
                self.visit(step, value=True, **kwargs)
            else:
                self.add_code(('ldc', step))

            # Make the increment
            if hasattr(node, 'down') and node.down:
                self.add_code(('sub',))
            else:
                self.add_code(('add',))

            # Store counter value
            self.add_code(('stv', i, j))

            # Reload counter value and compare to end
            self.add_code(('ldv', i, j))
            if hasattr(node, 'down') and node.down:
                self.visit(node.start, value=True, **kwargs)
                self.add_code(('gre',))
            else:
                self.visit(node.end, value=True, **kwargs)
                self.add_code(('leq',))
        else:  # Code for counter initialization
            if hasattr(node, 'down') and node.down:
                start = node.end
            else:
                start = node.start
            self.visit(start, value=True, **kwargs)
            self.add_code(('stv', i, j))

    def visit_RangeEnumeration(self, node, inc=False, **kwargs):
        i, j = self.env.lookup_offset(node.counter.identifier)
        if inc:  # Code for increment
            # Load values for counter and step
            self.add_code([('ldv', i, j), ('ldc', 1)])

            # Make the increment
            if hasattr(node, 'down') and node.down:
                self.add_code(('sub',))
            else:
                self.add_code(('add',))

            # Store counter value
            self.add_code(('stv', i, j))

            # Reload counter value and compare to range end
            self.add_code(('ldv', i, j))
            if hasattr(node, 'down') and node.down:
                self.visit(node.mode.bounds.lower, value=True, **kwargs)
                self.add_code(('gre',))
            else:
                self.visit(node.mode.bounds.upper, value=True, **kwargs)
                self.add_code(('leq',))
        else:  # Code for initialization
            if hasattr(node, 'down') and node.down:
                self.visit(node.mode.bounds.upper, value=True, **kwargs)
            else:
                self.visit(node.mode.bounds.lower, value=True, **kwargs)
            self.add_code(('stv', i, j))

    def visit_ExitAction(self, node, **kwargs):
        _l = self.env.lookup_sym(node.label.identifier)
        self.add_code(('jmp', _l['label']))

    def visit_DeclarationStatement(self, node, **kwargs):
        for d in node.dcls:
            self.visit(d, **kwargs)

    def visit_Declaration(self, node, **kwargs):
        if hasattr(node, 'init'):
            for v in node.ids:
                # Generate code for expression
                self.visit(node.init, value=True, **kwargs)

                # Store value
                i, j = self.env.lookup_offset(v.identifier)
                self.add_code(('stv', i, j))

    def visit_SynonymStatement(self, node, **kwargs):
        return  # Nothing to do here

    def visit_NewModeStatement(self, node, **kwargs):
        return  # Nothing to do here

    def visit_ActionStatement(self, node, **kwargs):
        self.visit(node.action, **kwargs)

        # Create label
        if hasattr(node, 'label'):
            _l = self.env.lookup_sym(node.label.identifier)
            self.add_code(('lbl', _l['label']))
