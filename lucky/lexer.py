# lexer.py
# Project 1 - MC911
# luck - Lya Ultra Compiler Kit
#
# Lexical & Syntactic Analysis of Lya language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Lucky lexer description, using Python's ply library.
"""

import ply.lex as lex


class Lexer(object):
    # TODO(any): Docstring describing the parser class

    # Reserved and predefined words
    reserved = {
        'array': 'ARRAY',
        'by': 'BY',
        'chars': 'CHARS',
        'dcl': 'DCL',
        'do': 'DO',
        'down': 'DOWN',
        'else': 'ELSE',
        'elsif': 'ELSIF',
        'end': 'END',
        'exit': 'EXIT',
        'fi': 'FI',
        'for': 'FOR',
        'if': 'IF',
        'in': 'IN',
        'loc': 'LOC',
        'type': 'TYPE',
        'od': 'OD',
        'proc': 'PROC',
        'ref': 'REF',
        'result': 'RESULT',
        'return': 'RETURN',
        'returns': 'RETURNS',
        'syn': 'SYN',
        'then': 'THEN',
        'to': 'TO',
        'while': 'WHILE',
        'bool': 'BOOL',
        'char': 'CHAR',
        'false': 'FALSE',
        'int': 'INT',
        'length': 'LENGTH',
        'lower': 'LOWER',
        'null': 'NULL',
        'num': 'NUM',
        'pred': 'PRED',
        'print': 'PRINT',
        'read': 'READ',
        'succ': 'SUCC',
        'true': 'TRUE',
        'upper': 'UPPER'
    }

    # Tokens that will be used by the parser
    tokens = ['PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD', 'EQ', 'ASSIGN',
              'SEMI', 'LPAREN', 'RPAREN', 'LBRACKET', 'RBRACKET', 'AND', 'OR',
              'NOT', 'NEQ', 'GT', 'GEQ', 'LT', 'LEQ', 'CAT', 'CCONST',
              'SCONST', 'ICONST', 'ID', 'COMMA', 'COLON', 'ARROW'] \
        + list(reserved.values())

    # Ignored characters
    t_ignore = ' \t'

    # --- Define tokens ---
    # When building the master regular expression, rules are added in the
    # following order:
    #
    #   - All tokens defined by functions are added in the same order as they
    #   appear in the lexer file.
    #   - Tokens defined by strings are added next by sorting them in order of
    #   decreasing regular expression length (longer expressions are added
    #   first).

    # -- String tokens
    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_MOD = r'%'
    t_EQ = r'=='
    t_ASSIGN = r'='
    t_SEMI = r';'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACKET = r'\['
    t_RBRACKET = r'\]'
    t_AND = r'&&'
    t_OR = r'\|\|'
    t_NOT = r'!'
    t_NEQ = r'!='
    t_GT = r'>'
    t_GEQ = r'>='
    t_LT = r'<'
    t_LEQ = r'<='
    t_CAT = r'&'
    t_CCONST = r'"(.|\^\(\d+\))"'
    t_SCONST = r'"[^"]*"'
    t_COMMA = r','
    t_COLON = r':'
    t_ARROW = r'->'

    # -- Function tokens
    # Increase lineno on newlines
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    # Check if identifier is a reserved word
    def t_ID(self, t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        t.type = self.reserved.get(t.value, 'ID')
        return t

    # Parse integer constant
    def t_ICONST(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    # Ignore line comment
    def t_LCOMMENT(self, t):
        r'//.*'
        pass

    # Ignore block comment
    def t_BCOMMENT(self, t):
        r'/\*(.|\n)*?\*/'
        t.lexer.lineno += t.value.count('\n')

    # -- Error handling rules
    # TODO(alemedeiros): Implement column number and see if it is possible to
    #   print an clang-like error message <3
    def t_unterminatedString(self, t):
        r'"""(.|\n)*?'
        print('lineno %d : Unterminated string' % t.lexer.lineno)
        t.lexer.skip(1)

    def t_unterminatedString2(self, t):
        r"'''(.|\n)*?"
        print('lineno %d : Unterminated string' % t.lexer.lineno)
        t.lexer.skip(1)

    def t_unterminatedComment(self, t):
        r'/\*(.|\n)*?'
        print('lineno %d : Unterminated comment' % t.lexer.lineno)
        t.lexer.skip(1)

    def t_error(self, t):
        print('lineno %d : Undefined error' % t.lexer.lineno)
        t.lexer.skip(1)

    # Build the lexer with this classes rules
    def build(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)


def _main():
    l = Lexer()
    l.build()
    lex.runmain(lexer=l.lexer)

# If script is running as main code, run a lexer test, either tokenize the
# file given in command line argument or standard input
if __name__ == '__main__':
    _main()
