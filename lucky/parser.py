# parser.py
# Project 1 - MC911
# luck - Lya Ultra Compiler Kit
#
# Lexical & Syntactic Analysis of Lya language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Lucky parser description, using Python's ply library.
"""

import argparse
import sys

from ply import yacc

from . import ast, lexer, visitors


class Parser(object):
    # TODO(any): Docstring describing the parser class
    # TODO(alemedeiros): Create verbose level
    tokens = lexer.Lexer.tokens

    # -- Program rules
    def p_program(self, p):
        """ program : statement_list """
        p[0] = ast.Program(p[1])

    def p_statement_list(self, p):
        """ statement_list : statement
                           | statement_list statement """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 3:
            p[0] = p[1] + [p[2]]

    def p_statement(self, p):
        """ statement : declaration_statement
                      | synonym_statement
                      | newmode_statement
                      | procedure_statement
                      | action_statement """
        p[0] = p[1]

    # -- Declaration statement rules
    def p_declaration_statement(self, p):
        """ declaration_statement : DCL declaration_list SEMI """
        p[0] = ast.DeclarationStatement(p[2])

    def p_declaration_list(self, p):
        """ declaration_list : declaration
                             | declaration_list COMMA declaration """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_declaration(self, p):
        """ declaration : identifier_list mode
                        | identifier_list mode initialization """
        if len(p) == 3:
            p[0] = ast.Declaration(p[1], p[2])
        elif len(p) == 4:
            p[0] = ast.Declaration(p[1], p[2], init=p[3])

    def p_initialization(self, p):
        """ initialization : ASSIGN expression """
        p[0] = p[2]

    def p_identifier_list(self, p):
        """ identifier_list : ID
                            | identifier_list COMMA ID """
        if len(p) == 2:
            p[0] = [ast.Identifier(p[1])]
        elif len(p) == 4:
            p[0] = p[1] + [ast.Identifier(p[3])]

    # -- Synonym statement rules
    def p_synonym_statement(self, p):
        """ synonym_statement : SYN synonym_list SEMI """
        p[0] = ast.SynonymStatement(p[2])

    def p_synonym_list(self, p):
        """ synonym_list : synonym_definition
                         | synonym_list COMMA synonym_definition """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_synonym_definition(self, p):
        """ synonym_definition : identifier_list ASSIGN expression
                               | identifier_list mode ASSIGN expression """
        if len(p) == 4:
            p[0] = ast.Synonym(p[1], p[3])
        elif len(p) == 5:
            p[0] = ast.Synonym(p[1], p[4], mode=p[2])

    # -- New mode statement rules
    def p_newmode_statement(self, p):
        """ newmode_statement : TYPE newmode_list SEMI """
        p[0] = ast.NewModeStatement(p[2])

    def p_newmode_list(self, p):
        """ newmode_list : mode_definition
                         | newmode_list COMMA mode_definition """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_mode_definition(self, p):
        """ mode_definition : identifier_list ASSIGN mode """
        p[0] = ast.NewMode(p[1], p[3])

    def p_mode(self, p):
        """ mode : discrete_mode
                 | reference_mode
                 | composite_mode """
        p[0] = p[1]

    def p_mode_id(self, p):
        """ mode : ID """
        p[0] = ast.CustomMode(ast.Identifier(p[1]))

    def p_discrete_mode_default(self, p):
        """ discrete_mode : INT
                          | BOOL
                          | CHAR """
        p[0] = ast.DiscreteMode(p[1])

    def p_discrete_mode_range(self, p):
        """ discrete_mode : discrete_mode LPAREN literal_range RPAREN """
        p[0] = ast.DiscreteRangeMode(p[1], p[3])

    def p_discrete_mode_range_id(self, p):
        """ discrete_mode : ID LPAREN literal_range RPAREN """
        p[0] = ast.DiscreteRangeMode(ast.Identifier(p[1]), p[3])

    def p_literal_range(self, p):
        """ literal_range : expression COLON expression """
        p[0] = ast.Range(p[1], p[3])

    def p_reference_mode(self, p):
        """ reference_mode : REF mode """
        p[0] = ast.ReferenceMode(p[2])

    def p_composite_mode(self, p):
        """ composite_mode : string_mode
                           | array_mode """
        p[0] = p[1]

    def p_string_mode(self, p):
        """ string_mode : CHARS LBRACKET integer_literal RBRACKET """
        p[0] = ast.StringMode(p[3])

    def p_array_mode(self, p):
        """ array_mode : ARRAY LBRACKET index_mode_list RBRACKET mode """
        p[0] = ast.ArrayMode(p[3], p[5])

    def p_index_mode_list(self, p):
        """ index_mode_list : index_mode
                            | index_mode_list COMMA index_mode """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_index_mode_range(self, p):
        """ index_mode : literal_range """
        p[0] = p[1]

    # -- Assignment statement rules
    def p_location(self, p):
        """ location : dereferenced_reference
                     | indexed_element
                     | slice
                     | call_action """
        p[0] = p[1]

    def p_location_id(self, p):
        """ location : ID """
        p[0] = ast.LocationName(ast.Identifier(p[1]))

    def p_slice(self, p):
        """ slice : ID LBRACKET literal_range RBRACKET """
        p[0] = ast.Slice(ast.Identifier(p[1]), p[3])

    def p_indexed_element(self, p):
        """ indexed_element : ID LBRACKET expression_list RBRACKET """
        p[0] = ast.IndexedElement(ast.Identifier(p[1]), p[3])

    def p_dereferenced_reference(self, p):
        """ dereferenced_reference : location ARROW """
        p[0] = ast.DereferencedReference(p[1])

    def p_expression_list(self, p):
        """ expression_list : expression
                            | expression_list COMMA expression """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    # -- Literals
    def p_primitive_value(self, p):
        """ primitive_value : literal
                            | parenthesized_expression """
        p[0] = p[1]

    def p_literal(self, p):
        """ literal : integer_literal
                    | boolean_literal
                    | character_literal
                    | empty_literal
                    | character_string_literal """
        p[0] = p[1]

    def p_integer_literal(self, p):
        """ integer_literal : ICONST """
        p[0] = ast.Literal('int', p[1])

    def p_boolean_literal(self, p):
        """ boolean_literal : FALSE
                            | TRUE """
        p[0] = ast.Literal('bool', p[1] == 'true')

    def p_character_literal(self, p):
        """ character_literal : CCONST """
        p[0] = ast.Literal('char', p[1])

    def p_empty_literal(self, p):
        """ empty_literal : NULL """
        p[0] = ast.Literal('ref', p[1])

    def p_character_string_literal(self, p):
        """ character_string_literal : SCONST """
        p[0] = ast.Literal('string', p[1])

    # -- Expression rules
    def p_parenthesized_expression(self, p):
        """ parenthesized_expression : LPAREN expression RPAREN """
        p[0] = p[2]

    def p_expression(self, p):
        """ expression : operand0
                       | conditional_expression """
        p[0] = p[1]

    def p_conditional_expression(self, p):
        """ conditional_expression : IF expression then_expression else_expression FI
                                   | IF expression then_expression elsif_expression else_expression FI """
        if len(p) == 6:
            p[0] = ast.ConditionalExpression(p[2], p[3], p[4])
        elif len(p) == 7:
            p[0] = ast.ConditionalExpression(p[2], p[3], p[5], elsif=p[4])

    def p_then_expression(self, p):
        """ then_expression : THEN expression """
        p[0] = p[2]

    def p_else_expression(self, p):
        """ else_expression : ELSE expression """
        p[0] = p[2]

    def p_elsif_expression(self, p):
        """ elsif_expression : ELSIF expression then_expression
                             | elsif_expression ELSIF expression then_expression """
        if len(p) == 4:
            p[0] = ast.ElsifExpression(p[2], p[3])
        elif len(p) == 5:
            p[0] = ast.ElsifExpression(p[3], p[4], elsif=p[1])

    # -- Operand rules
    def p_operand0(self, p):
        """ operand0 : operand1
                     | operand0 operator1 operand1 """
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 4:
            p[0] = ast.BinaryOperation(p[2], p[1], p[3])

    def p_operator1(self, p):
        """ operator1 : relational_operator
                      | membership_operator """
        p[0] = p[1]

    def p_relational_operator(self, p):
        """ relational_operator : AND
                                | OR
                                | EQ
                                | NEQ
                                | GT
                                | GEQ
                                | LT
                                | LEQ """
        p[0] = p[1]

    def p_membership_operator(self, p):
        """ membership_operator : IN """
        p[0] = p[1]

    def p_operand1(self, p):
        """ operand1 : operand2
                     | operand1 operator2 operand2 """
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 4:
            p[0] = ast.BinaryOperation(p[2], p[1], p[3])

    def p_operator2(self, p):
        """ operator2 : arithmetic_additive_operator
                      | string_concatenation_operator """
        p[0] = p[1]

    def p_arithmetic_additive_operator(self, p):
        """ arithmetic_additive_operator : PLUS
                                         | MINUS """
        p[0] = p[1]

    def p_string_concatenation_operator(self, p):
        """ string_concatenation_operator : CAT """
        p[0] = p[1]

    def p_operand2(self, p):
        """ operand2 : operand3
                     | operand2 arithmetic_multiplicative_operator operand3 """
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 4:
            p[0] = ast.BinaryOperation(p[2], p[1], p[3])

    def p_arithmetic_multiplicative_operator(self, p):
        """ arithmetic_multiplicative_operator : TIMES
                                               | DIVIDE
                                               | MOD """
        p[0] = p[1]

    def p_operand3(self, p):
        """ operand3 : operand4
                     | MINUS operand4
                     | NOT operand4 """
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 3:
            p[0] = ast.UnaryOperation(p[1], p[2])

    def p_operand4(self, p):
        """ operand4 : location
                     | referenced_location
                     | primitive_value """
        p[0] = p[1]

    def p_referenced_location(self, p):
        """ referenced_location : ARROW location """
        p[0] = ast.ReferencedLocation(p[2])

    # -- General action statements rules
    def p_action_statement(self, p):
        """ action_statement : action SEMI
                             | ID COLON action SEMI """
        if len(p) == 3:
            p[0] = ast.ActionStatement(p[1])
        elif len(p) == 5:
            p[0] = ast.ActionStatement(p[3], label=ast.Identifier(p[1]))

    def p_action(self, p):
        """ action : if_action
                   | do_action
                   | assignment_action
                   | call_action
                   | exit_action
                   | return_action
                   | result_action """
        p[0] = p[1]

    def p_assignment_action(self, p):
        """ assignment_action : location ASSIGN expression
                              | location dyadic_operator ASSIGN expression """
        if len(p) == 4:
            p[0] = ast.AssignmentAction(p[1], p[3])
        elif len(p) == 5:
            # Expand the syntax-sugar
            expr = ast.BinaryOperation(p[2], p[1], p[4])
            p[0] = ast.AssignmentAction(p[1], expr)

    def p_dyadic_operator(self, p):
        """ dyadic_operator : arithmetic_additive_operator
                            | arithmetic_multiplicative_operator
                            | string_concatenation_operator """
        p[0] = p[1]

    # -- Flux control rules
    def p_if_action(self, p):
        """ if_action : IF expression then_clause FI """
        p[0] = ast.IfAction(p[2], p[3])

    def p_if_action_else(self, p):
        """ if_action : IF expression then_clause else_clause FI """
        p[0] = ast.IfAction(p[2], p[3], else_clause=p[4])

    def p_if_action_elsif(self, p):
        """ if_action : IF expression then_clause elsif_clause FI """
        p[0] = ast.IfAction(p[2], p[3], elsif=p[4])

    def p_if_action_elsif_else(self, p):
        """ if_action : IF expression then_clause elsif_clause else_clause FI """
        p[0] = ast.IfAction(p[2], p[3], elsif=p[4], else_clause=p[5])

    def p_then_clause(self, p):
        """ then_clause : THEN action_statement_list """
        p[0] = p[2]

    def p_else_clause(self, p):
        """ else_clause : ELSE action_statement_list """
        p[0] = p[2]

    def p_elsif_clause(self, p):
        """ elsif_clause : ELSIF expression then_clause
                         | ELSIF expression then_clause elsif_clause """
        if len(p) == 4:
            p[0] = ast.ElsifAction(p[2], p[3])
        elif len(p) == 5:
            p[0] = ast.ElsifAction(p[2], p[3], elsif=p[4])

    def p_action_statement_list(self, p):
        """ action_statement_list : empty
                                  | action_statement_list action_statement """
        if len(p) == 2:
            p[0] = []
        elif len(p) == 3:
            p[0] = p[1] + [p[2]]

    # -- Loop related rules
    def p_do_action(self, p):
        """ do_action : DO action_statement_list OD """
        p[0] = ast.DoAction(p[2])

    def p_do_action_while(self, p):
        """ do_action : DO while_control SEMI action_statement_list OD """
        p[0] = ast.DoAction(p[4], cond=p[2])

    def p_do_action_for(self, p):
        """ do_action : DO for_control SEMI action_statement_list OD """
        p[0] = ast.DoAction(p[4], for_cond=p[2])

    def p_do_action_for_while(self, p):
        """ do_action : DO for_control while_control SEMI action_statement_list OD """
        p[0] = ast.DoAction(p[5], for_cond=p[2], cond=p[3])

    def p_for_control(self, p):
        """ for_control : FOR step_enumeration
                        | FOR range_enumeration """
        p[0] = p[2]

    def p_step_enumeration(self, p):
        """ step_enumeration : ID ASSIGN start_value end_value
                             | ID ASSIGN start_value step_value DOWN end_value """
        if len(p) == 5:
            p[0] = ast.StepEnumeration(ast.Identifier(p[1]), p[3], p[4],
                                       down=False)
        elif len(p) == 7:
            p[0] = ast.StepEnumeration(ast.Identifier(p[1]), p[3], p[6],
                                       step=p[4], down=True)

    def p_step_enumeration_step_value(self, p):
        """ step_enumeration : ID ASSIGN start_value step_value end_value """
        p[0] = ast.StepEnumeration(ast.Identifier(p[1]), p[3], p[5], step=p[4],
                                   down=False)

    def p_step_enumeration_down(self, p):
        """ step_enumeration : ID ASSIGN start_value DOWN end_value """
        p[0] = ast.StepEnumeration(ast.Identifier(p[1]), p[3], p[5], down=True)

    def p_start_value(self, p):
        """ start_value : expression """
        p[0] = p[1]

    def p_step_value(self, p):
        """ step_value : BY expression """
        p[0] = p[2]

    def p_end_value(self, p):
        """ end_value : TO expression """
        p[0] = p[2]

    # TODO(alemedeiros): check whether it should really be discrete_mode_name
    def p_range_enumeration(self, p):
        """ range_enumeration : ID IN discrete_mode
                              | ID DOWN IN discrete_mode """
        if len(p) == 4:
            p[0] = ast.RangeEnumeration(ast.Identifier(p[1]), p[3], down=False)
        elif len(p) == 5:
            p[0] = ast.RangeEnumeration(ast.Identifier(p[1]), p[4], down=True)

    def p_while_control(self, p):
        """ while_control : WHILE expression """
        p[0] = p[2]

    # -- Procedure calling and return rules
    def p_call_action(self, p):
        """ call_action : procedure_call
                        | builtin_call """
        p[0] = p[1]

    def p_procedure_call(self, p):
        """ procedure_call : ID LPAREN RPAREN
                           | ID LPAREN parameter_list RPAREN"""
        if len(p) == 4:
            p[0] = ast.CallAction(ast.Identifier(p[1]), [])
        elif len(p) == 5:
            p[0] = ast.CallAction(ast.Identifier(p[1]), p[3])

    def p_parameter_list(self, p):
        """ parameter_list : parameter
                           | parameter_list COMMA parameter"""
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_parameter(self, p):
        """ parameter : expression """
        p[0] = p[1]

    def p_exit_action(self, p):
        """ exit_action : EXIT ID """
        p[0] = ast.ExitAction(ast.Identifier(p[2]))

    def p_return_action(self, p):
        """ return_action : RETURN
                          | RETURN expression """
        if len(p) == 2:
            p[0] = ast.ReturnAction()
        elif len(p) == 3:
            p[0] = ast.ReturnAction(result=p[2])

    def p_result_action(self, p):
        """ result_action : RESULT expression """
        p[0] = ast.ResultAction(p[2])

    def p_builtin_call(self, p):
        """ builtin_call : builtin_name LPAREN RPAREN
                         | builtin_name LPAREN parameter_list RPAREN """
        if len(p) == 4:
            p[0] = ast.CallAction(p[1], [])
        elif len(p) == 5:
            p[0] = ast.CallAction(p[1], p[3])

    def p_builtin_name(self, p):
        """ builtin_name : NUM
                         | PRED
                         | SUCC
                         | UPPER
                         | LOWER
                         | LENGTH
                         | READ
                         | PRINT """
        p[0] = ast.Identifier(p[1])

    # -- Procedure declaration rules
    def p_procedure_statement(self, p):
        """ procedure_statement : ID COLON procedure_definition SEMI """
        p[0] = ast.ProcedureStatement(ast.Identifier(p[1]), p[3])

    def p_procedure_definition(self, p):
        """ procedure_definition : PROC LPAREN RPAREN SEMI statement_list END
                                 | PROC LPAREN formal_parameter_list RPAREN result_spec SEMI statement_list END """
        if len(p) == 7:
            p[0] = ast.Procedure([], p[5])
        elif len(p) == 9:
            p[0] = ast.Procedure(p[3], p[7], result=p[5])

    def p_procedure_definition_result(self, p):
        """ procedure_definition : PROC LPAREN RPAREN result_spec SEMI statement_list END """
        p[0] = ast.Procedure([], p[6], result=p[4])

    def p_procedure_definition_params(self, p):
        """ procedure_definition : PROC LPAREN formal_parameter_list RPAREN SEMI statement_list END """
        p[0] = ast.Procedure(p[3], p[6])

    def p_formal_parameter_list(self, p):
        """ formal_parameter_list : formal_parameter
                                  | formal_parameter_list COMMA formal_parameter """
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]

    def p_formal_parameter(self, p):
        """ formal_parameter : identifier_list mode
                             | identifier_list mode LOC """
        if len(p) == 3:
            p[0] = ast.FormalParameter(p[1], p[2], loc=False)
        elif len(p) == 4:
            p[0] = ast.FormalParameter(p[1], p[2], loc=True)

    def p_result_spec(self, p):
        """ result_spec : RETURNS LPAREN mode RPAREN
                        | RETURNS LPAREN mode LOC RPAREN """
        if len(p) == 5:
            p[0] = (p[3], False)
        elif len(p) == 6:
            p[0] = (p[3], True)

    def p_empty(self, p):
        """ empty : """
        pass

    def p_error(self, p):
        # TODO(alemedeiros): properly do error handling
        print('syntax error:', p, file=sys.stderr)

    # Build the parser with the class rules
    def build(self, **kwargs):
        # TODO(alemedeiros): Docstring
        self.parser = yacc.yacc(module=self)

    def parse(self, prgm, lex, **kwargs):
        """
        Parse the given Program using the given Lexer, assumes the Parser has
        already been built.

            prgm:
                input program string

            lex:
                Lexer class (assumes it wasn't built)
        """
        lex.build()
        return self.parser.parse(prgm, lexer=lex.lexer)


def _main():
    """
    Test mode for Lucky Parser from Lya Ultra Compiler Kit (luck).
    """
    arg_parse = argparse.ArgumentParser(description=_main.__doc__)
    arg_parse.add_argument('file', metavar='FILE', type=str,
                           help='Lya source file')
    arg_parse.add_argument('-p', '--print_mode',
                           choices=['default', 'magic', 'dot'],
                           default='default',
                           help='Format of the output AST representation')
    arg_parse.add_argument('-o', '--output_format',
                           choices=['svg', 'pdf', 'png'], default='svg',
                           help='Output format for the dot print mode')
    args = arg_parse.parse_args()

    p = Parser()
    p.build()
    l = lexer.Lexer()

    # Get input from first positional argument
    f = open(args.file, 'r')
    prgm_ast = p.parse(f.read(), l)

    if args.print_mode == 'default':
        v = visitors.ShowAstVisitor()
        v.visit(prgm_ast, indent=0)
    elif args.print_mode == 'magic':
        v = visitors.MagicVisitor()
        v.visit(prgm_ast, depth=0)
    elif args.print_mode == 'dot':
        v = visitors.DotVisitor(args.file)
        v.visit(prgm_ast)
        v.dot.format = args.output_format
        v.dot.render()

# If script is running as main code, run a parser test file given in command
# line argument or standard input
if __name__ == '__main__':
    _main()
