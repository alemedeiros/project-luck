# semantic.py
# Project 2 - MC911
# luck - Lya Ultra Compiler Kit
#
# Semantic & Code Generation of Lya Language
#
# 115966 - Alexandre Novais de Medeiros
# 117044 - Giovanna Madella de Luca
"""
Semantic verification related code. Definition of a Symbol Table and
Environment, alongside with proper visitor used to decorate the AST.
"""

from . import visitors
from . import ast

# import pprint
import sys


class SymbolTable(dict):
    """
    Class representing a symbol table. It should
    provide functionality for adding and looking
    up nodes associated with identifiers.
    """
    def __init__(self, decl=None):
        super().__init__()
        self.decl = decl

    def add(self, name, value):
        self[name] = value

    def lookup(self, name):
        return self.get(name, None)

    def return_type(self):
        if self.decl:
            return self.decl.mode
        return None


class SemanticCheckVisitor(visitors.NodeVisitor):
    """
    Visitor class to check AST's semantic.
    """
    bool_op = set(['==', '!=', '>', '>=', '<', '<='])

    def __init__(self):
        self.env = Environment()
        self.error = False
        self.proc_labels = 0
        self.str_ind = 0
        self.h = []

    def get_type_from_mode(self, mode):
        if isinstance(mode, ast.StringMode):
            length_type = self.get_type_from_mode(mode.length)

            if length_type != IntType():
                print('>>> Semantic error string must have int length', file=sys.stderr)
                self.error = True
            if not hasattr(mode.length, 'value'):
                print('>>> Semantic error cannot compute string length', file=sys.stderr)
                self.error = True
                mode.length.value = 0

            return StringType(mode.length.value)

        if isinstance(mode, ast.DiscreteMode):
            if mode.mode == "int":
                return IntType()
            elif mode.mode == "char":
                return CharType()
            elif mode.mode == "bool":
                return BoolType()
            else:
                pass  # Error, unexpected value

        if isinstance(mode, ast.ReferenceMode):
            return ReferenceType(self.get_type_from_mode(mode.mode))

        if isinstance(mode, ast.ArrayMode):
            inds = []
            for ind in mode.indexes:
                lower_type = self.visit(ind.lower)
                upper_type = self.visit(ind.upper)

                if lower_type != IntType() or upper_type != IntType():
                    print('>>> Semantic error invalid array indexes', file=sys.stderr)
                    self.error = True

                if not (hasattr(ind.lower, 'value') and
                        hasattr(ind.upper, 'value')):
                    print('>>> Semantic error cannot compute array size', file=sys.stderr)
                    self.error = True
                else:
                    inds.append((ind.lower.value, ind.upper.value))

            return ArrayType(self.get_type_from_mode(mode.mode), inds)

        if isinstance(mode, ast.CustomMode):
            return self.env.lookup(mode.identifier.identifier)

        # We don't really need to check the ranges, so just return the type
        if isinstance(mode, ast.DiscreteRangeMode):
            lower_type = self.visit(mode.bounds.lower)
            upper_type = self.visit(mode.bounds.upper)
            if lower_type != IntType() or upper_type != IntType():
                print('>>> Semantic error invalid range type', file=sys.stderr)
                self.error = True
            # Commented out because, inconsistently, when this is in a for,
            # bounds don't need to have their value known at compile time
            # if not (hasattr(mode.bounds.lower, 'value') and
            #         hasattr(mode.bounds.upper, 'value')):
            #     print('>>> Semantic error cannot compute range bounds', file=sys.stderr)
            #     self.error = True

            return self.get_type_from_mode(mode.mode)

    def add_to_symbol_table(self, node, newmode=False, loc=False):
        m = self.get_type_from_mode(node.mode)

        for i in node.ids:
            if newmode:
                self.env.add_local(i.identifier,  m)
            else:
                self.env.add_local(i.identifier, {'type': m, 'loc': loc})

        print('==== DEBUG: changed scope ====', file=sys.stderr)
        for sc in self.env.stack:
            print('>>>> Scope', file=sys.stderr)
            for k, v in sc.items():
                if isinstance(v, dict):
                    print('\t', k, ':', file=sys.stderr)
                    for _k, _v in v.items():
                        print('\t\t', _k, ':', _v, file=sys.stderr)
                else:
                    print('\t', k, ':', v, file=sys.stderr)

        return [(i.identifier, m, loc) for i in node.ids]

    def visit_Declaration(self, node):
        self.add_to_symbol_table(node)

    def visit_NewMode(self, node):
        self.add_to_symbol_table(node, newmode=True)

    def visit_Synonym(self, node):
        value_type = self.visit(node.expr)

        if hasattr(node, 'mode'):
            def_mode = self.get_type_from_mode(node.mode)
            if def_mode != value_type:
                print('>>> Semantic error: incorrect type', file=sys.stderr)
                self.error = True

        # Add Synonym to symbol table
        if not hasattr(node.expr, 'value'):
            print('>>> Semantic error: No value for synonym', file=sys.stderr)
            self.error = True

            # Continue semantic analysis to (maybe) find other errors
            for i in node.ids:
                self.env.add_local(i.identifier, {'type': value_type})
        else:
            for i in node.ids:
                self.env.add_local(i.identifier, {'type': value_type,
                                                  'syn': True,
                                                  'value': node.expr.value})

        print('==== DEBUG: changed scope ====', file=sys.stderr)
        for sc in self.env.stack:
            print('>>>> Scope', file=sys.stderr)
            for k, v in sc.items():
                if isinstance(v, dict):
                    print('\t', k, ':', file=sys.stderr)
                    for _k, _v in v.items():
                        print('\t\t', _k, ':', _v, file=sys.stderr)
                else:
                    print('\t', k, ':', v, file=sys.stderr)

    def offset_size(self, elem):
        if isinstance(elem, dict):
            if 'type' in elem:
                # If has value, its value is known at compile time, no need to
                # keep it in the runtime stack
                if 'syn' in elem:
                    return 0

                if 'loc' in elem and elem['loc']:
                    return 1

                if isinstance(elem['type'], (IntType, CharType, BoolType,
                                             ReferenceType)):
                    return 1
                elif isinstance(elem['type'], StringType):
                    return elem['type'].length + 1
                elif isinstance(elem['type'], ArrayType):
                    array_size = 1
                    for lower, upper in elem['type'].indexes:
                        array_size = (upper - lower + 1) * array_size
                    return array_size

        return 0

    def visit_Program(self, node):
        self.env.push('global')  # Create a new scope

        # Visit program statements
        for stmt in node.stmts:
            self.visit(stmt)

        # Compute global variables offsets
        offset = 0
        node.offsets = {}
        for k, v in self.env.peek().items():
            if k in node.offsets:
                continue
            _off = self.offset_size(v)
            if _off == 0:
                continue
            node.offsets[k] = offset
            offset += _off

        node.vars_offset = offset
        node.sym_table = self.env.peek()

        print('==== DEBUG: global offsets', file=sys.stderr)
        for k, v in node.offsets.items():
            print('\t', k, v, file=sys.stderr)

        node.last_label = self.proc_labels

        self.env.pop()  # Remove scope from environment

        node.h = self.h
        return node

    def visit_ProcedureStatement(self, node):
        self.env.push(node.label.identifier)  # Create a new scope

        # Add procedure parameters to new scope
        proc_params = []
        for p in node.definition.params:
            proc_params += self.add_to_symbol_table(p, loc=p.loc)

        # Determine parameters offset (start on -2)
        node.offsets = {}
        offset = -2
        for i, m, l in reversed(proc_params):
            _off = self.offset_size({'type': m, 'loc': l})
            node.offsets[i] = offset - _off
            offset -= _off

        # Arguments offset was computed as negative, since they were stacked
        # before the call, this value already considers
        node.args_offset = - (offset + 2)
        node.level = self.env.scope_level()

        # Add procedure signature to previous scope
        if hasattr(node.definition, 'result'):
            result_mode, loc = node.definition.result
            proc_return = self.get_type_from_mode(result_mode)

            # Compute return value offset
            _off = self.offset_size({'type': proc_return, 'loc': loc})
            node.offsets['__result__'] = offset - _off
        else:
            proc_return = NoType
            loc = False

        # Assign label to procedure
        proc_label = self.proc_labels
        self.proc_labels += 1
        node.proc_label = proc_label

        params_types = [(m, l) for _, m, l in proc_params]
        self.env.add_previous(node.label.identifier,
                              {'return': proc_return,
                               'loc': loc,
                               'params': params_types,
                               'label': proc_label,
                               'offset': node.args_offset - 2})

        # Visit procedures statements
        for stmt in node.definition.stmts:
            self.visit(stmt)

        # Compute procedure scope offsets
        offset = 0
        for k, v in self.env.peek().items():
            if k in node.offsets:
                continue
            _off = self.offset_size(v)
            if _off == 0:
                continue
            node.offsets[k] = offset
            offset += _off

        node.vars_offset = offset
        node.sym_table = self.env.peek()

        print('==== DEBUG: procedure', node.label.identifier, 'offsets', file=sys.stderr)
        for k, v in node.offsets.items():
            print('\t', k, v, file=sys.stderr)

        self.env.pop()  # Remove procedure scope from environment

    def visit_ActionStatement(self, node):
        if hasattr(node, 'label'):
            # Assign label to statement
            action_label = self.proc_labels
            self.proc_labels += 1
            node.action_label = action_label
            self.env.add_local(node.label.identifier, {'label': action_label})

        self.visit(node.action)

    def visit_ExitAction(self, node):
        _l = self.env.lookup(node.label.identifier)
        if 'label' not in _l:
            print('>>> Semantic error: undefined label', file=sys.stderr)
            self.error = True

    def visit_ConditionalExpression(self, node):
        # Check Condition type (must be Boolean)
        cond_type = self.visit(node.cond)
        if cond_type != BoolType():
            print('>>> Semantic error: if condition must be boolean', file=sys.stderr)
            self.error = True

        # Check if clauses type are the same
        then_type = self.visit(node.then)
        else_type = self.visit(node.else_clause)
        if then_type != else_type:
            print('>>> Semantic error: incompatible type of expressions', file=sys.stderr)
            self.error = True

        # TODO(alemedeiros): elsif clause verification

        return then_type

    def compute_unary(self, op, v):
        if op == '!':
            return not v
        if op == '+':
            return v
        if op == '-':
            return -v
        # Error :(

    def visit_UnaryOperation(self, node):
        operand_type = self.visit(node.operand)
        if node.op not in operand_type.un_ops:
            print('>>> Semantic error: invalid operator', node.op, file=sys.stderr)
            self.error = True

        # Compute value if we can
        if hasattr(node.operand, 'value'):
            node.value = self.compute_unary(node.op, node.operand.value)

        # Save type on AST node
        node.sem_type = operand_type

        return operand_type

    def compute_binary(self, op, left, right):
        p_ops = set(['+', '-', '*', '%', '==', '!=', '>', '>=', '<', '<='])
        if op in p_ops:
            return eval(str(left) + op + str(right))
        if op == '/':
            return left // right
        if op == '||':
            return left or right
        if op == '&&':
            return left and right
        if op == '&':
            return left + right

    def visit_BinaryOperation(self, node):
        left_type = self.visit(node.left)
        right_type = self.visit(node.right)

        print('==== DEBUG: op:', node.op, file=sys.stderr)
        print('==== DEBUG: left_type:', left_type, file=sys.stderr)
        print('==== DEBUG: right_type:', right_type, file=sys.stderr)

        # TODO(alemedeiros): 'in' operator

        if left_type != right_type:
            print('>>> Semantic error: both operators must be of same type', file=sys.stderr)
            self.error = True

        if node.op not in left_type.bin_ops:
            print('>>> Semantic error: invalid operator', node.op, file=sys.stderr)
            self.error = True

        # Compute value if we can
        if hasattr(node.left, 'value') and hasattr(node.right, 'value'):
            node.value = self.compute_binary(node.op, node.left.value,
                                             node.right.value)

        # Comparisson operators always return boolean
        if node.op in self.bool_op:
            node.sem_type = BoolType()
            return node.sem_type
        else:
            node.sem_type = left_type
            return left_type

    def visit_Literal(self, node):
        if node.mode == 'string':
            node.index = self.str_ind
            self.str_ind += 1
            self.h.append(node.value)

            print('$$$$ DEBUG: new str in H:', node.value, file=sys.stderr)
            print('$$$$ DEBUG: index is:', self.str_ind - 1, file=sys.stderr)

        _t = self.env.lookup(node.mode)
        node.sem_type = _t
        return _t

    def visit_LocationName(self, node):
        _loc = self.env.lookup(node.identifier.identifier)
        print('== DEBUG:', node.identifier.identifier, _loc, file=sys.stderr)
        if 'value' in _loc:
            node.value = _loc['value']

        if 'loc' in _loc:
            node.loc = _loc['loc']
        else:
            node.loc = False

        node.sem_type = _loc['type']
        return _loc['type']

    def visit_DereferencedReference(self, node):
        _type = self.visit(node.ref)
        node.sem_type = _type
        return _type

    def literal(self, value):
        return ast.Literal('int', value)

    def visit_IndexedElement(self, node):
        _t = self.env.lookup(node.identifier.identifier)
        array_type = _t['type']
        node.sem_type = array_type.elem_type

        # TODO(alemedeiros): Check if symbol table returned an array type or
        # string

        if 'loc' in _t:
            node.loc = _t['loc']
        else:
            node.loc = False

        for i in node.indexes:
            i_type = self.visit(i)
            if i_type != IntType():
                print('>>> Semantic error: invalid index', file=sys.stderr)
                self.error = True

        # Convert indexes to expression
        _lower = self.literal(array_type.indexes[0][0])
        abs_ind = ast.BinaryOperation('-', node.indexes[0], _lower)
        cur = 1
        for i in node.indexes[1:]:
            last_lower, last_upper = array_type.indexes[cur-1]
            _size = last_upper - last_lower + 1
            size = self.literal(_size)
            _tmp = ast.BinaryOperation('*', size, abs_ind)

            cur_lower = self.literal(array_type.indexes[cur][0])
            cur_ind = ast.BinaryOperation('-', i, cur_lower)

            abs_ind = ast.BinaryOperation('+', _tmp, cur_ind)

            cur += 1

        # Maybe we can use constant propagation on indexes
        self.visit(abs_ind)

        node.index_expr = abs_ind
        # node._fields.append('index_expr')

        return array_type.elem_type

    def visit_Slice(self, node):
        slice_type = self.env.lookup(node.identifier.identifier)['type']
        node.sem_type = slice_type

        # TODO(alemedeiros): Check if symbol table returned an array type or
        # string

        lower_type = self.visit(node.bounds.lower)
        upper_type = self.visit(node.bounds.upper)

        if lower_type != IntType() or upper_type != IntType():
            print('>>> Semantic error slice index must be an integer', file=sys.stderr)
            self.error = True

        # Check slice bounds
        if not (hasattr(node.bounds.lower, 'value') and
                hasattr(node.bounds.upper, 'value')):
            print('>>> Semantic error cannot compute slide range', file=sys.stderr)
            self.error = True

        return slice_type

    def visit_AssignmentAction(self, node):
        loc_type = self.visit(node.loc)
        expr_type = self.visit(node.expr)

        if loc_type != expr_type:
            print('>>> Semantic error: invalid types on assignment', file=sys.stderr)
            self.error = True

    def visit_CallAction(self, node):
        _name = node.proc.identifier
        proc_type = self.env.lookup(_name)
        node.sem_type = proc_type

        # Check if procedure exists / is a procedure
        if proc_type is None:
            print('>>> Semantic error: undefined procedure', _name, file=sys.stderr)
            self.error = True
        if not isinstance(proc_type, dict):
            # TODO: fix it
            print('>>> Semantic error:', _name, 'not a procedure', file=sys.stderr)
            self.error = True

        # Check parameters
        for i, p in enumerate(node.params):
            _t = self.visit(p)
            if node.proc.identifier in ['print', 'read']:
                continue
            exp_t, _l = proc_type['params'][i]
            if _l:
                p.loc = True
                if not isinstance(p, ast.LocationName):
                    print('>>> Semantic error: expected variable', file=sys.stderr)
                    self.error = True
            else:
                p.loc = False
            if exp_t != _t:
                print('>>> Semantic error: invalid procedure parameters', file=sys.stderr)
                self.error = True

        if 'loc' in proc_type:
            node.loc = proc_type['loc']
        else:
            node.loc = False

        return proc_type['return']

    def verify_if(self, node):
        cond_type = self.visit(node.cond)
        if cond_type != BoolType():
            print('>>> Semantic error: if condition must be boolean', file=sys.stderr)
            self.error = True

        # Check Elsif
        if hasattr(node, 'elsif'):
            self.visit(node.elsif)

        # Visit statements
        for stmt in node.then:
            self.visit(stmt)
        if hasattr(node, 'else_clause'):
            for stmt in node.else_clause:
                self.visit(stmt)

    def visit_IfAction(self, node):
        self.verify_if(node)

    def visit_ElsifAction(self, node):
        self.verify_if(node)

    def visit_DoAction(self, node):
        # Check while-cond
        if hasattr(node, 'cond'):
            cond_type = self.visit(node.cond)
            if cond_type != BoolType():
                print('>>> Semantic error: while condition must be boolean', file=sys.stderr)
                self.error = True

        # Check for-cond
        if hasattr(node, 'for_cond'):
            self.visit(node.for_cond)

        # Check do action statements
        for stmt in node.stmts:
            self.visit(stmt)

    def visit_StepEnumeration(self, node):
        start_type = self.visit(node.start)
        end_type = self.visit(node.end)

        # TODO: confirm if start/end values must be known at compile time

        # check start and end types (maybe should just be Int)
        if start_type != end_type:
            print('>>> Semantic error: incompatible start/end type', file=sys.stderr)
            self.error = True

        if hasattr(node, 'step'):
            step_type = self.visit(node.step)
            if start_type != step_type:
                print('>>> Semantic error: invalid step type', file=sys.stderr)
                self.error = True

        # Add loop counter to symbol table
        self.env.add_local(node.counter.identifier, {'type': start_type})

    def visit_RangeEnumeration(self, node):
        range_type = self.get_type_from_mode(node.mode)

        # TODO: confirm if range values must be known at compile time
        # TODO: check range type

        # Add loop counter to symbol table
        self.env.add_local(node.counter.identifier, {'type': range_type})

    def check_result(self, node):
        result_type = self.visit(node.result)

        # Get current procedure definition
        proc = self.env.lookup(self.env.peek().decl)

        # Check if return type is compatible with procedure definition
        if result_type != proc['return']:
            print('>>> Semantic error: invalid return type', file=sys.stderr)
            self.error = True

        if 'loc' in proc:
            node.loc = proc['loc']
            if not isinstance(node.result, ast.LocationName):
                print('>>> Semantic error: expected location name', file=sys.stderr)
        else:
            node.loc = False

    def visit_ResultAction(self, node):
        self.check_result(node)

    def visit_ReturnAction(self, node):
        if hasattr(node, 'result'):
            self.check_result(node)


class Environment(object):
    def __init__(self):
        self.stack = []
        self.root = SymbolTable()
        self.stack.append(self.root)
        self.root.update({
            "int": IntType(),
            "char": CharType(),
            "string": StringType(),
            "bool": BoolType(),
            "pred": {"return": IntType(), "params": [(IntType(), False)]},
            "succ": {"return": IntType(), "params": [(IntType(), False)]},
            "length": {"return": IntType(), "params": [(ArrayType(AnyType()), False)]},
            "read": {"return": NoType(), "params": []},
            "print": {"return": NoType(), "params": []},
            "num": {"return": IntType(), "params": [(CharType(), False)]},
            "upper": {"return": IntType(), "params": [(ArrayType(AnyType()), False)]},
            "lower": {"return": IntType(), "params": [(ArrayType(AnyType()), False)]}

        })

    def push(self, procedure_name):
        self.stack.append(SymbolTable(decl=procedure_name))

    def pop(self):
        self.stack.pop()

    def peek(self):
        return self.stack[-1]

    def scope_level(self):
        return len(self.stack)

    def add_local(self, name, value):
        self.peek().add(name, value)

    def add_previous(self, name, value):
        self.stack[-2].add(name, value)

    def add_root(self, name, value):
        self.root.add(name, value)

    def lookup(self, name):
        for scope in reversed(self.stack):
            hit = scope.lookup(name)
            if hit is not None:
                return hit
        return None

    def find(self, name):
        if name in self.stack[-1]:
            return True
        else:
            return False


class Type(object):
    bin_ops = set()
    un_ops = set()


class NoType(Type):
    def __eq__(self, other):
        return False

    def __str__(self):
        return "NoType"


class AnyType(Type):
    def __eq__(self, other):
        return True

    def __str__(self):
        return "AnyType"


class ReferenceType(Type):
    def __init__(self, ref_type):
        self.ref_type = ref_type

    def __eq__(self, other):
        if isinstance(other, ReferenceType):
            return self.ref_type == other.ref_type
        else:
            return False

    def __str__(self):
        return "ReferenceType (" + str(self.ref_type) + ")"


class ArrayType(Type):
    def __init__(self, elem_type, indexes=[]):
        self.elem_type = elem_type
        self.indexes = indexes
        if indexes:
            self.length = indexes[0][1] - indexes[0][0] + 1

    def __eq__(self, other):
        if isinstance(other, ArrayType):
            return self.elem_type == other.elem_type
        else:
            return False

    def __str__(self):
        return ("ArrayType (" + str(self.elem_type) + ") [" +
                str(self.indexes) + "]")


class StringType(Type):
    # TODO: discover if 'concat' operator is '+' or '&'
    bin_ops = set(['==', '!=', '&'])

    def __init__(self, length=0):
        self.length = length

    def __eq__(self, other):
        return isinstance(other, StringType)

    def __str__(self):
        return "StringType [" + str(self.length) + "]"


class IntType(Type):
    bin_ops = set(['+', '-', '*', '/', '%', '==', '!=', '>', '>=', '<', '<='])
    un_ops = set(['+', '-'])

    def __eq__(self, other):
        return isinstance(other, IntType)

    def __str__(self):
        return "IntType"


class CharType(Type):
    bin_ops = set(['==', '!=', '>', '>=', '<', '<='])

    def __eq__(self, other):
        return isinstance(other, CharType)

    def __str__(self):
        return "CharType"


class BoolType(Type):
    bin_ops = set(['==', '!=', '||', '&&'])
    un_ops = set('!')

    def __eq__(self, other):
        return isinstance(other, BoolType)

    def __str__(self):
        return "BoolType"
